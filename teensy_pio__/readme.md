# Pickup Firmware
Firmware for the custom built motion pickup devices.

### Contents
- `/src` current sources
- `/lib` unused
- `/archived` deprecated code, kept for future reference.

### Interesting Files
- `/src/air_01.*` main function.

### Build
PlatformIO Core toolchain with Teensy 3.2 target

__Target:__ Teensy 3.x Microcontroller
__Toolchain:__ PlatformIO