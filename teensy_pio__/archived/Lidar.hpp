//
// Created by Gerold Schneider on 07.12.17.
//

#ifndef TEENSY_PIO_D_VL6180X_H
#define TEENSY_PIO_D_VL6180X_H

#include <Arduino.h>
#include <Adafruit_VL6180X.h>
#include "Oled.hpp"
#include "AirI2c.hpp"

class Lidar: public Adafruit_VL6180X
{
private:
    static const uint16_t m_debugUpdateRate = 500; //ms
    uint_fast32_t m_debugCount = 0;
    uint_fast32_t m_rangeStartTime = 0;
    uint_fast32_t m_rangeDeltaTime = 0;
    uint_fast32_t m_rangeSum = 0;
    uint_fast32_t m_rangeCount = 0;
    uint_fast32_t m_iter = 0;
public:
    static Lidar *Sensor;
    static void setup();
    void init();
    void hook();
    void write8(uint16_t address, uint8_t data);
    uint_fast8_t read8(uint16_t address);
};


#endif //TEENSY_PIO_D_VL6180X_H
