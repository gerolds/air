#include "Lidar.hpp"

Lidar *(Lidar::Sensor);

void Lidar::setup() {
    Serial.println("Adafruit VL6180x test!");
    Oled::Singleton->push("VL6180x");

    Sensor = new Lidar();

    Sensor->init();
}

void Lidar::hook() {
    uint_fast8_t lidar_range = 0;
    uint_fast8_t lidar_range_status = 0; // errorcodes and ready for measurement flag (1)
    uint_fast8_t lidar_gpio_status = 0; // interrupt events, sample ready flag (4)
    uint_fast8_t lidar_range_error = 0;

    lidar_range_status = read8(VL6180X_REG_RESULT_RANGE_STATUS); // errorcodes and ready for measurement flag (1)
    lidar_gpio_status = read8(VL6180X_REG_RESULT_INTERRUPT_STATUS_GPIO); // interrupt events, sample ready flag (4)
    lidar_range_error = lidar_range_status >> 4;

    // poll for available sample
    if (lidar_gpio_status & 0x04) {

        // read the sample
        lidar_range = read8(VL6180X_REG_RESULT_RANGE_VAL);
        m_rangeDeltaTime = millis() - m_rangeStartTime;
        m_rangeSum += m_rangeDeltaTime;
        m_rangeCount++;

        // clear interrupt
        write8(VL6180X_REG_SYSTEM_INTERRUPT_CLEAR, 0x07);
    }

    // sensor ready for new measurement?
    if (lidar_range_status & 0x01) {
        // trigger measurement
        write8(VL6180X_REG_SYSRANGE_START, 0x01);
        m_rangeStartTime = millis();
    }


    // update display
    m_debugCount = millis();
    if (millis() - m_debugCount > m_debugUpdateRate) {
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->println("                    ");

        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Range: ");
        Oled::Singleton->print(lidar_range);
        Oled::Singleton->print("mm ");
        Oled::Singleton->print((float)m_rangeCount / (float)m_rangeSum, 2);
        Oled::Singleton->print("Hz ");

        Serial.print("Range: ");
        Serial.print(lidar_range);
        Serial.println(" mm");
        m_rangeSum = 0;
        m_rangeCount = 0;
    }

    // Some error occurred, print it out!
    if  ((lidar_range_error >= VL6180X_ERROR_SYSERR_1) && (lidar_range_status <= VL6180X_ERROR_SYSERR_5)) {
        Serial.println("System error");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("System error ");
        Oled::Singleton->print(lidar_range_status);
    }
    else if (lidar_range_status == VL6180X_ERROR_ECEFAIL) {
        Serial.println("ECE failure");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("ECE failure");
    }
    else if (lidar_range_status == VL6180X_ERROR_NOCONVERGE) {
        Serial.println("No convergence");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("No convergence");
    }
    else if (lidar_range_status == VL6180X_ERROR_RANGEIGNORE) {
        Serial.println("Ignoring range");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Ignoring range");

    }
    else if (lidar_range_status == VL6180X_ERROR_SNR) {
        Serial.println("Signal/Noise error");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Signal/Noise error");

    }
    else if (lidar_range_status == VL6180X_ERROR_RAWUFLOW) {
        Serial.println("Raw reading underflow");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Raw read underflow");

    }
    else if (lidar_range_status == VL6180X_ERROR_RAWOFLOW) {
        Serial.println("Raw reading overflow");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Raw read overflow");

    }
    else if (lidar_range_status == VL6180X_ERROR_RANGEUFLOW) {
        Serial.println("Range reading underflow");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Range read underflow");

    }
    else if (lidar_range_status == VL6180X_ERROR_RANGEOFLOW) {
        Serial.println("Range reading overflow");
        Oled::Singleton->setCursor(0, 8 * 4);
        Oled::Singleton->print("Range read overflow");

    }
    m_rangeDeltaTime = millis() - m_rangeStartTime;
    Serial.print("∂t: ");
    Serial.println(m_rangeDeltaTime);
}


 //write 1 byte
void Lidar::write8(uint16_t address, uint8_t data)
{
    Wire.beginTransmission(VL6180X_DEFAULT_I2C_ADDR);
    Wire.write(address >> 8);
    Wire.write(address);
    Wire.write(data);
    Wire.endTransmission();
}

uint_fast8_t Lidar::read8(uint16_t address)
{
    //uint8_t data;

    Wire.beginTransmission(VL6180X_DEFAULT_I2C_ADDR);
    Wire.write(address >> 8);
    Wire.write(address);
    Wire.endTransmission();

    Wire.requestFrom(VL6180X_DEFAULT_I2C_ADDR, (uint8_t)1);
    uint_fast8_t r = (uint_fast8_t)Wire.read();

    return r;
}

void Lidar::init()
{
    if (!Lidar::Sensor->begin()) {
        Serial.println("Failed to find sensor");
        Oled::Singleton->push("| connection failed");
        delay(5000);
    } else {

        String tmp = "";
        tmp += "| connected @ ";
        tmp += String(VL6180X_DEFAULT_I2C_ADDR, HEX);
        Oled::Singleton->push(tmp);
        delay(500);
    }
}
