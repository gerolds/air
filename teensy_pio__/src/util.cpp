#include "util.h"

// stores the input vector in a buffer and subtracts the average
// over all buffer entries from the most recent entry, then
// updates the output pointers to that smoothed value.
void smooth_follow(
    const int buf_len,
    const int x_in,
    const int y_in,
    const int z_in,
    int *counter,
    int x_buf[],
    int y_buf[],
    int z_buf[],
    long *x_out,
    long *y_out,
    long *z_out
)
{
    int x_sum = 0;
    int y_sum = 0;
    int z_sum = 0;

    x_buf[*counter % buf_len] = x_in;
    y_buf[*counter % buf_len] = y_in;
    z_buf[*counter % buf_len] = z_in;
    for (int i = 0; i < buf_len; i++) {
        x_sum += x_buf[i];
        y_sum += y_buf[i];
        z_sum += z_buf[i];
    }

    *x_out = x_in - x_sum / buf_len;
    *y_out = y_in - y_sum / buf_len;
    *z_out = z_in - z_sum / buf_len;

    *counter += 1;
}

Quaternion Util::qInverse(const Quaternion a)
{
    return {a.w, -a.x, -a.y, -a.z};
}

Quaternion Util::qTransform(const Quaternion a, const Quaternion b)
{
    qMult(qInverse(a), b);
}

Quaternion Util::qMult(Quaternion a, Quaternion b)
{
    return {
        a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y,
        a.w * b.y + a.y * b.w + a.z * b.x - a.x * b.z,
        a.w * b.z + a.z * b.w + a.x * b.y - a.y * b.x,
        a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z,
    };
}

Vector3 Util::qMult(Quaternion quat, Vector3 vec)
{
    float num = quat.x * 2.0f;
    float num2 = quat.y * 2.0f;
    float num3 = quat.z * 2.0f;
    float num4 = quat.x * num;
    float num5 = quat.y * num2;
    float num6 = quat.z * num3;
    float num7 = quat.x * num2;
    float num8 = quat.x * num3;
    float num9 = quat.y * num3;
    float num10 = quat.w * num;
    float num11 = quat.w * num2;
    float num12 = quat.w * num3;
    Vector3 result;
    result.x = (1.0f - (num5 + num6)) * vec.x + (num7 - num12) * vec.y + (num8 + num11) * vec.z;
    result.y = (num7 + num12) * vec.x + (1.0f - (num4 + num6)) * vec.y + (num9 - num10) * vec.z;
    result.z = (num8 - num11) * vec.x + (num9 + num10) * vec.y + (1.0f - (num4 + num5)) * vec.z;
    return result;
}

float Util::norm(const float x, const float y, const float z)
{
    return sqrtf(x * x + y * y + z * z);
}

float Util::norm(const Vector3 a)
{
    return sqrtf(a.x * a.x + a.y * a.y + a.z * a.z);
}

float Util::distance(const float ax,
                     const float ay,
                     const float az,
                     const float bx,
                     const float by,
                     const float bz)
{
    float dx, dy, dz = 0;
    dx = ax - bx;
    dy = ay - by;
    dz = az - bz;
    return norm(dx, dy, dz);
}

float Util::distance(const Vector3 a, const Vector3 b)
{
    Vector3 d = {
        a.x - b.x,
        a.y - b.y,
        a.z - b.z
    };
    return norm(d);
}

float Util::dot(const float ax,
                const float ay,
                const float az,
                const float bx,
                const float by,
                const float bz)
{
    return ax * bx + ay * by + az * bz;
}

float Util::dot(const Vector3 a, const Vector3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

Vector3 Util::cross(const Vector3 a, const Vector3 b)
{
    return {
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x,
    };
}