const unsigned int GY10_BUF = 1500;
const unsigned int GY61_POLL_PERIOD = 10000; // µs
const unsigned int GY61_RESET_PERIOD = 15000000; // µs

unsigned long last_gy61_poll = 0;
unsigned long last_gy61_reset = 0;
int gy61_x_buf[GY10_BUF];
int gy61_y_buf[GY10_BUF];
int gy61_z_buf[GY10_BUF];
int gy61_x_velocity_buf[GY10_BUF];
int gy61_y_velocity_buf[GY10_BUF];
int gy61_z_velocity_buf[GY10_BUF];
int gy61_follow_counter = 0;
int gy61_velocity_follow_counter = 0;
int gy61_x = 0;
int gy61_y = 0;
int gy61_z = 0;
int gy61_scale = 0;
int gy61_xvsy = 0;
long gy61_calib_x = 484;
long gy61_calib_y = 480;
long gy61_calib_z = 600;

int gy61_zero(int v) {
    return v - 512;
}

void gy61_follow(
    const int buf_len,
    int *counter,
    const int gy61_x_read,
    const int gy61_y_read,
    const int gy61_z_read,
    int gy61_x_buf[],
    int gy61_y_buf[],
    int gy61_z_buf[],
    int* gy61_x,
    int* gy61_y,
    int* gy61_z
) {
    int x_sum = 0;
    int y_sum = 0;
    int z_sum = 0;

    gy61_x_buf[*counter % buf_len] = gy61_x_read;
    gy61_y_buf[*counter % buf_len] = gy61_y_read;
    gy61_z_buf[*counter % buf_len] = gy61_z_read;
    for (int i = 0; i < buf_len; i++) {
        x_sum += gy61_x_buf[i];
        y_sum += gy61_y_buf[i];
        z_sum += gy61_z_buf[i];
    }

    *gy61_x = gy61_x_read - x_sum / buf_len;
    *gy61_y = gy61_y_read - y_sum / buf_len;
    *gy61_z = gy61_z_read - z_sum / buf_len;

    *counter += 1;
}

// dt in microseconds
// x,y,z in range -500...500 where 500 is equivalent to +-3 * 9.81 µm/µs^2
// x_,y_,z_velocity in µm/µs
void gy61_velocity(
    const long delta_time,
    const long x,
    const long y,
    const long z,
    long *x_velocity,
    long *y_velocity,
    long *z_velocity) {
    *x_velocity += x / delta_time;
    *y_velocity += y / delta_time;
    *z_velocity += z / delta_time;
}

// dt in microseconds
// x_,y_,z_velocity in µm/µs
// x_,y_,z_dist in µm
void gy61_distance(
    const long delta_time,
    const long x_velocity,
    const long y_velocity,
    const long z_velocity,
    long *x_dist,
    long *y_dist,
    long *z_dist) {
    *x_dist += delta_time * x_velocity;
    *y_dist += delta_time * y_velocity;
    *z_dist += delta_time * z_velocity;
}

/*  from loop()
    if (micros() - last_sample > sample_period) {
    last_sample = micros();
    float vol = 0;
    vol = min( 500.0, max( 0.0, (float)gy61_scale / 2.0 - 10.0) ) * 4;

    int f1 = 23;
    int f2 = 26;
    int f3 = 29;

    sample = ((float)(micros() % (int)periods[f1]) / periods[f1] * 0.25);
    sample += ((float)(micros() % (int)periods[f2]) / periods[f2] * 0.45);
    sample += ((float)(micros() % (int)periods[f3]) / periods[f3] * 0.75);

    // sample += ((float)(micros() % (periods[fq])) / (float)periods[fq]);
    analogWrite(A22, int(sample * (float)vol));

    //analogWrite(A22, int(sample * float(vol)));
    //analogWrite(A22, 1000);
    //delay(3);
    analogWrite(A22, 0);
    //Serial.println(int(sample * float(vol)));
    }

    if ((micros() - last_gy61_poll) > GY61_POLL_PERIOD) {
    last_gy61_poll = micros();
    smooth_follow(
      GY10_BUF,
      &gy61_follow_counter,
      analogRead(A9),
      analogRead(A8),
      analogRead(A7),
      gy61_x_buf,
      gy61_y_buf,
      gy61_z_buf,
      &gy61_x,
      &gy61_y,
      &gy61_z
    );

    gy61_velocity(
      delta_time,
      gy61_x,
      gy61_y,
      gy61_z,
      &x_velocity,
      &y_velocity,
      &z_velocity
    );


    smooth_follow(
      GY10_BUF,
      x_velocity, y_velocity, z_velocity,
      &gy61_velocity_follow_counter,
      gy61_x_velocity_buf, gy61_y_velocity_buf, gy61_z_velocity_buf,
      &x_velocity, &y_velocity, &z_velocity
    );


    gy61_scale = sqrt(gy61_z * gy61_z + gy61_y * gy61_y + gy61_x * gy61_x);
    gy61_xvsy = sqrt(gy61_x * gy61_x / gy61_y * gy61_y);

    gy61_distance(
      delta_time,
      x_velocity, y_velocity, z_velocity,
      &x_dist, &y_dist, &z_dist
    );
    }
*/


