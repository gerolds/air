//
// Created by Gerold Schneider on 07.12.17.
//

#ifndef TEENSY_PIO_UTIL_H
#define TEENSY_PIO_UTIL_H


#include <math.h>

void smooth_follow(
    const int buf_len,
    const int x_in,
    const int y_in,
    const int z_in,
    int *counter,
    int x_buf[],
    int y_buf[],
    int z_buf[],
    long *x_out,
    long *y_out,
    long *z_out
);

struct Quaternion
{
    float x;
    float y;
    float z;
    float w;
};

struct Vector3
{
    float x;
    float y;
    float z;
};

class Util
{
public:
    static float distance(float ax, float ay, float az, float bx, float by, float bz);
    static float distance(const Vector3 a, const Vector3 b);
    static float dot(float ax, float ay, float az, float bx, float by, float bz);
    static float dot(const Vector3 a, const Vector3 b);
    static Vector3 cross(const Vector3 a, const Vector3 b);
    static float norm(float x, float y, float z);
    static Quaternion qMult(Quaternion a, Quaternion b);
    static float norm(const Vector3 a);
    static Quaternion qInverse(const Quaternion a);
    static Quaternion qTransform(const Quaternion a, const Quaternion b);
    Vector3 qMult(Quaternion quat, Vector3 vec);
};

#endif //TEENSY_PIO_UTIL_H
