//
// Created by Gerold Schneider on 06.12.17.
//

#ifndef TEENSY_PIO_D_I2C_H
#define TEENSY_PIO_D_I2C_H

#include <Arduino.h>
#include <i2c_t3.h>

class AirI2c: public i2c_t3
{
public:
    AirI2c(uint8_t i2c_bus);
    static void setup();
    static void scan(i2c_t3 *bus);
    static int clearBus(uint8_t SDA, uint8_t SCL);
};


#endif //TEENSY_PIO_D_I2C_H
