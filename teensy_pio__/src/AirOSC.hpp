//
// Created by Gerold Schneider on 07.12.17.
//

#ifndef TEENSY_PIO_D_OSC_H
#define TEENSY_PIO_D_OSC_H

#include <Arduino.h>
#include <OSCBoards.h>
#include <OSCBundle.h>
#include <OSCData.h>
#include <OSCMatch.h>
#include <OSCMessage.h>
#include <OSCTiming.h>
#include <AirImu.h>
#include <SLIPEncodedUSBSerial.h>
#include "Oled.hpp"
#include "util.h"

#define OSCSend true
#define OSC_SEND_RATE 50000; // µs

class AirOSC
{
public:
    static void setup();
    static SLIPEncodedUSBSerial *Slip;
    static AirOSC *Singleton;
    void init();
    void hook();
    AirOSC();
    float m_byteRate; // m_slipByteSum / display_interval
    uint32_t m_byteMsgAvg;
    float m_msgRate;
private:
    uint32_t m_oscDeltaTime;        // used to control OSC output rate
    uint32_t m_oscLastMessage;            // used to control OSC output rate
    uint32_t m_slipByteSum;
    uint32_t m_slipSumCount;
    uint32_t m_displayDeltaTime;
    uint32_t m_displayLastUpdate;
    const uint32_t display_interval = DISPLAY_UPDATE_RATE; // µs
    uint32_t m_slipElapsed;
    float m_msgMaxRate;
    unsigned int m_msgCount;
};


#endif //TEENSY_PIO_D_OSC_H
