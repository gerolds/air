#include <i2c_t3.h>
#include "AirI2c.hpp"
#include "Oled.hpp"

void AirI2c::setup()
{
    String tmp = "";

    Oled::Singleton->push("Preparing I2C");
    int err;
    err = AirI2c::clearBus(18, 19);
    if (err) {
        tmp = "I2C_0 error ";
        tmp += String(err);
        Oled::Singleton->push(tmp);
        delay(5000);
    }
    else {
        Oled::Singleton->push("I2C_0 clear");
    };
    err = AirI2c::clearBus(29, 30);
    if (err) {
        tmp = "I2C_1 error ";
        tmp += String(err);
        Oled::Singleton->push(tmp);
        delay(5000);
    }
    else {
        Oled::Singleton->push("I2C_1 clear");
    }

    //  TWBR = 12;  // 400 kbit/sec I2C speed for Pro Mini
    // Setup for Master mode, pins 18/19, external pullups, 400kHz for Teensy 3.5
    Wire.begin(I2C_MASTER, 0x00, I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_400); // teensy 3.2 and 3.5
#if defined(__MK20DX256__) // teensy 3.1
    Wire1.begin(I2C_MASTER, 0x00, I2C_PINS_29_30, I2C_PULLUP_EXT, I2C_RATE_400);
#elif defined(__MK64FX512__) // teensy 3.5
    Wire1.begin(I2C_MASTER, 0x00, I2C_PINS_33_34, I2C_PULLUP_EXT, I2C_RATE_400);
#endif
    Wire.setDefaultTimeout(1000000); // microseconds
    Wire1.setDefaultTimeout(1000000);
    delay(1000);
}

void AirI2c::scan(i2c_t3 *bus)
{
    // scan for i2c devices
    byte error, error1, address;
    int nDevices;
    String tmp = "";

    String busId = String(bus->bus);
    Serial.println("Scanning...");
    tmp = "Scan I2C Bus #";
    tmp += busId;
    Oled::Singleton->push(tmp);
    nDevices = 0;
    for (address = 1; address < 127; address++) {
        // The i2c_scanner uses the return value of
        // the Write.endTransmisstion to see if
        // a device did acknowledge to the address.
        bus->beginTransmission(address);
        error = bus->endTransmission();

        if (error == 0) {
            Serial.print("I2C device found at address 0x");
            Serial.print(address, HEX);
            Serial.println("on bus A");

            tmp = "|";
            tmp += busId;
            tmp += ":0x";
            if (address < 16) tmp += "0";

            switch (address) {
                case 0x69:
                case 0x68:
                    tmp += String(address, HEX);
                    tmp += ":MPU9250";
                    break;
                case 0xC:
                    tmp += String(address, HEX);
                    tmp += ":AK8963";
                    break;
                case 0x29:
                    tmp += String(address, HEX);
                    tmp += ":VL6180X";
                    break;
                default:
                    tmp += String(address, HEX);
                    break;
            }
            Oled::Singleton->push(tmp);

            nDevices++;
        }
        else if (error == 4) {
            Serial.print("Unknow error at address 0x");
            tmp = "|bus";
            tmp += busId;
            tmp += " error ";
            tmp += String(error);
            tmp += " @0x";
            if (address < 16) tmp += "0";
            Serial.print(address, HEX);
            Serial.println(" on bus0");
            tmp += String(address, HEX);
            Oled::Singleton->push(tmp);
        }
    }
    if (nDevices == 0) {
        Serial.println("No I2C devices found\n");
        Oled::Singleton->push("|no devices found");
    }
    else {
        Serial.println("done\n");
    }
}

AirI2c::AirI2c(uint8_t i2c_bus)
    : i2c_t3(i2c_bus)
{ }

/**
 * I2C_ClearBus
 * (http://www.forward.com.au/pfod/ArduinoProgramming/I2C_ClearBus/index.html)
 * (c)2014 Forward Computing and Control Pty. Ltd.
 * NSW Australia, www.forward.com.au
 * This code may be freely used for both private and commerical use
 *
 *
 * This routine turns off the I2C bus and clears it
 * on return SCA and SCL pins are tri-state inputs.
 * You need to call Wire.begin() after this to re-enable I2C
 * This routine does NOT use the Wire library at all.
 *
 * returns 0 if bus cleared
 *         1 if SCL held low.
 *         2 if SDA held low by slave clock stretch for > 2sec
 *         3 if SDA held low after 20 clocks.
 */
int AirI2c::clearBus(uint8_t SDA, uint8_t SCL)
{
#if defined(TWCR) && defined(TWEN)
    TWCR &= ~(_BV(TWEN)); //Disable the Atmel 2-Wire interface so we can control the SDA and SCL pins directly
#endif

    pinMode(SDA, INPUT_PULLUP); // Make SDA (data) and SCL (clock) pins Inputs with pullup.
    pinMode(SCL, INPUT_PULLUP);

    delay(2500);  // Wait 2.5 secs. This is strictly only necessary on the first power
    // up of the DS3231 module to allow it to initialize properly,
    // but is also assists in reliable programming of FioV3 boards as it gives the
    // IDE a chance to start uploaded the program
    // before existing sketch confuses the IDE by sending Serial data.

    boolean SCL_LOW = (digitalRead(SCL) == LOW); // Check is SCL is Low.
    if (SCL_LOW) { //If it is held low Arduno cannot become the I2C master.
        return 1; //I2C bus error. Could not clear SCL clock line held low
    }

    boolean SDA_LOW = (digitalRead(SDA) == LOW);  // vi. Check SDA input.
    int clockCount = 20; // > 2x9 clock

    while (SDA_LOW && (clockCount > 0)) { //  vii. If SDA is Low,
        clockCount--;
        // Note: I2C bus is open collector so do NOT drive SCL or SDA high.
        pinMode(SCL, INPUT); // release SCL pullup so that when made output it will be LOW
        pinMode(SCL, OUTPUT); // then clock SCL Low
        delayMicroseconds(10); //  for >5uS
        pinMode(SCL, INPUT); // release SCL LOW
        pinMode(SCL, INPUT_PULLUP); // turn on pullup resistors again
        // do not force high as slave may be holding it low for clock stretching.
        delayMicroseconds(10); //  for >5uS
        // The >5uS is so that even the slowest I2C devices are handled.
        SCL_LOW = (digitalRead(SCL) == LOW); // Check if SCL is Low.
        int counter = 20;
        while (SCL_LOW && (counter > 0)) {  //  loop waiting for SCL to become High only wait 2sec.
            counter--;
            delay(100);
            SCL_LOW = (digitalRead(SCL) == LOW);
        }
        if (SCL_LOW) { // still low after 2 sec error
            return 2; // I2C bus error. Could not clear. SCL clock line held low by slave clock stretch for >2sec
        }
        SDA_LOW = (digitalRead(SDA) == LOW); //   and check SDA input again and loop
    }
    if (SDA_LOW) { // still low
        return 3; // I2C bus error. Could not clear. SDA data line held low
    }

    // else pull SDA line low for Start or Repeated Start
    pinMode(SDA, INPUT); // remove pullup.
    pinMode(SDA, OUTPUT);  // and then make it LOW i.e. send an I2C Start or Repeated start control.
    // When there is only one I2C master a Start or Repeat Start has the same function as a Stop and clears the bus.
    /// A Repeat Start is a Start occurring after a Start with no intervening Stop.
    delayMicroseconds(10); // wait >5uS
    pinMode(SDA, INPUT); // remove output low
    pinMode(SDA, INPUT_PULLUP); // and make SDA high i.e. send I2C STOP control.
    delayMicroseconds(10); // x. wait >5uS
    pinMode(SDA, INPUT); // and reset pins as tri-state inputs which is the default state on reset
    pinMode(SCL, INPUT);
    return 0; // all ok
}