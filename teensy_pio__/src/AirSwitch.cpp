#include "AirSwitch.h"

AirSwitch *(AirSwitch::Singleton);

void AirSwitch::setup()
{
    Singleton = new AirSwitch(UX_0_PIN, UX_1_PIN, PLAY_0_PIN, PLAY_1_PIN);
}

void AirSwitch::ux0IntHandler()
{
    AirSwitch::ux0 = digitalRead(m_ux0Pin);
}
void AirSwitch::ux1IntHandler()
{
    AirSwitch::ux1 = digitalRead(m_ux1Pin);
}

void AirSwitch::play0IntHandler()
{
    AirSwitch::play0 = digitalRead(m_play0Pin);
}

void AirSwitch::play1IntHandler()
{
    AirSwitch::play1 = digitalRead(m_play1Pin);
}

AirSwitch::AirSwitch(uint8_t ux_0_pin, uint8_t ux_1_pin, uint8_t play_0_pin, uint8_t play_1_pin)
    : m_ux0Pin(ux_0_pin),
      m_ux1Pin(ux_1_pin),
      m_play0Pin(play_0_pin),
      m_play1Pin(play_1_pin)
{
    pinMode(ux_0_pin, INPUT);
    pinMode(ux_1_pin, INPUT);
    pinMode(play_0_pin, INPUT);
    pinMode(play_1_pin, INPUT);
//    attachInterrupt(ux_0_pin, ux_0_jt, CHANGE);
//    attachInterrupt(ux_1_pin, ux_1_jt, CHANGE);
//    attachInterrupt(play_0_pin, play_0_jt, CHANGE);
//    attachInterrupt(play_1_pin, play_1_jt, CHANGE);
    init();
}

void AirSwitch::hook()
{
    ux0 = digitalRead(UX_0_PIN);
    ux1 = digitalRead(UX_1_PIN);
    play0 = digitalRead(PLAY_0_PIN);
    play1 = digitalRead(PLAY_1_PIN);
    if (micros() - m_lastUpdate > DISPLAY_UPDATE_RATE) {
        m_lastUpdate = micros();
        Oled::Singleton->setCursor(84, 8 * 5);
        Oled::Singleton->print(ux0);
        Oled::Singleton->print(ux1);
        Oled::Singleton->print(play0);
        Oled::Singleton->print(play1);
        Oled::Singleton->print("%");
    }
}

void AirSwitch::init()
{
    Oled::Singleton->setCursor(102, 8 * 6);
}

// static jump tables for interrupts

void AirSwitch::ux_0_jt()
{
    Singleton->ux0IntHandler();
}

void AirSwitch::ux_1_jt()
{
    Singleton->ux1IntHandler();
}

void AirSwitch::play_0_jt()
{
    Singleton->play0IntHandler();
}

void AirSwitch::play_1_jt()
{
    Singleton->play1IntHandler();
}
