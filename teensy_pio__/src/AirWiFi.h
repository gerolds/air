//
// Created by Gerold Schneider on 14.01.18.
//

#ifndef TEENSY_PIO_AIRWIFI_H
#define TEENSY_PIO_AIRWIFI_H

#include <Arduino.h>
#include <SPI.h>
#include <WiFi101.h>

class AirWiFi
{
public:
    static void setup();
    static AirWiFi *Singleton;
    void init();
    void hook();
    AirWiFi();
protected:
    static uint8_t *mac;
    static void printEncryptionType(int thisType);
};


#endif //TEENSY_PIO_AIRWIFI_H
