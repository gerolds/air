/* This code is based on the mpu9250 driver/library by Kris Winer (see note below)
 *
 * Primarily the original code was adapted into standard C++, removal of global
 * variables for class instantiation and addition of a configurable I2C bus
 * interface with the goal of making it fit to be used for multiple sensors in
 * the same project. This code is aimed at Teensy 3.2 and 3.5 microcontrollers.
 *
 * Added: Error Codes
 *
 * We are not using interrupts as we want to sample/poll the available
 * sensor data at a fixed rate accross all sensors, in a real-time application
 * and without any regard for power consuption.
 */

/*  MPU9250 & MS5637 AHRS
    by: Kris Winer
    date: April 1, 2014
    license: Beerware - Use this code however you'd like. If you
    find it useful you can buy me a beer some time.

    SDA and SCL should have external pull-up resistors (to 3.3V).
    4K7 resistors are on the MPU9250+MS5637 breakout board.

    Hardware setup:
    MPU9250 Breakout --------- Arduino
    VDD ---------------------- 3.3V
    VDDI --------------------- 3.3V
    SDA ----------------------- A4
    SCL ----------------------- A5
    GND ---------------------- GND

    Note: We are using the 400 kHz fast I2C mode by setting the TWI_FREQ  to 400000L /twi.h utility file.
*/

#include "AirImu.h"

AirImu *(AirImu::Sensor0);

AirImu *(AirImu::Sensor1);

uint32_t AirImu::m_viewLastUpdate = 0;

AirImu::AirImu(byte addr, uint8_t intPin, i2c_t3 *bus, String label)
{
    m_addr = addr;
    m_bus = bus;
    m_label = label;
    m_intPin = intPin; // INTERRUPTS ARE NOT USED!!!

    // global constants for 9 DoF fusion and AHRS (Attitude and Heading Reference System)
    // gyroscope measurement error in rads/s (start at 40 deg/s)
    m_gyroMeasError = (float) PI * (4.0f / 180.0f);
    // gyroscope measurement drift in rad/s/s (start at 0.0 deg/s/s)
    m_gyroMeasDrift = (float) PI * (0.0f / 180.0f);
    // There is a tradeoff in the beta parameter between accuracy and response speed.
    // In the original Madgwick study, beta of 0.041 (corresponding to GyroMeasError of 2.7 degrees/s) was found to give optimal accuracy.
    // However, with this value, the LSM9SD0 response time is about 10 seconds to a stable initial quaternion.
    // Subsequent changes also require a longish lag time to a stable output, not fast enough for a quadcopter or robot car!
    // By increasing beta (GyroMeasError) by about a factor of fifteen, the response time constant is reduced to ~2 sec
    // I haven't noticed any reduction in solution accuracy. This is essentially the I coefficient in a PID control sense;
    // the bigger the feedback coefficient, the faster the solution converges, usually at the expense of accuracy.
    // In any case, this is the free parameter in the Madgwick filtering and fusion scheme.
    // compute beta
    m_beta = (float) sqrt(3.0f / 4.0f) * m_gyroMeasError;
    // compute zeta, the other free parameter in the Madgwick scheme usually set to a small or zero value
    m_zeta = (float) sqrt(3.0f / 4.0f) * m_gyroMeasDrift;
    // these are the free parameters in the Mahony filter and fusion scheme, Kp for
    // proportional feedback, Ki for integral
    m_kp = 2.0f * 5.0f;
    m_ki = 0.0f;
}

void AirImu::interruptHandler()
{
    m_newData = true;
}

// Integrate the device into the local environment (called from the main setup()-routine)
void AirImu::setup()
{
    Oled::Singleton->push("IMU_0 Setup");
    // instantiate the driver class for imu 0
    Sensor0 = new AirImu(0x68, 17, &Wire, "IMU_0");
    // Set up the interrupt pin to active high, log-pull
    //pinMode(8, INPUT);

    Oled::Singleton->push("IMU_1 Setup");
    // instantiate the driver class for imu 1
    Sensor1 = new AirImu(0x68, 16, &Wire1, "IMU_1");
    // Set up the interrupt pin to active high, log-pull
    //pinMode(7, INPUT);

    // start initialization sequence
    // TODO: could this be parallelized in regard to required delays?
    Oled::Singleton->push("IMU_0 Init");
    Sensor0->init(&(AirImu::imu0_jt));

    // start initialization sequence
    Oled::Singleton->push("IMU_1 Init");
    Sensor1->init(&(AirImu::imu1_jt));
}

// Make the device ready for use
// returns 0 on success, otherwise an error code
int AirImu::init(void (*isr)())
{
    /*
     * Establish communication
     *
     */

    // Read the WHO_AM_I register, this is a good test of communication
    uint8_t c = readByte(m_bus, m_addr, WHO_AM_I_MPU9250);
    if (SerialDebug) {
        Serial.print("MPU9250 0 ");
        Serial.print("I AM ");
        Serial.print(c, HEX);
        Serial.print(" I should be ");
        Serial.println(0x71, HEX);
    }

    if (c == 0x71) // WHO_AM_I should always be 0x68
    {

        if (SerialDebug) Serial.println("MPU9250 on bus 0 is online...");
        m_tmp = "|";
        m_tmp += m_label;
        m_tmp += " Online";
        Oled::Singleton->push(m_tmp);

        /*
         * Initialize accelerometer and gyroscope
         *
         */

        MPU9250SelfTest(m_selfTest); // Start by performing self test and reporting values
        if (m_selfTest[0] < 14.
            && m_selfTest[1] < 14.
            && m_selfTest[2] < 14.
            && m_selfTest[3] < 14.
            && m_selfTest[4] < 14.
            && m_selfTest[5] < 14.) {
            // pass
            m_tmp = "|";
            m_tmp += m_label;
            m_tmp += " Test OK";
            Oled::Singleton->push(m_tmp);
        }
        else {
            m_tmp = "|";
            m_tmp += m_label;
            m_tmp += " ac/gy dev > 14%";
            Oled::Singleton->push(m_tmp);
            m_tmp = "|ax";
            m_tmp += String(m_selfTest[0], 1);
            m_tmp += "ay";
            m_tmp += String(m_selfTest[1], 1);
            m_tmp += "az";
            m_tmp += String(m_selfTest[2], 1);
            Oled::Singleton->push(m_tmp);
            m_tmp = "|gx";
            m_tmp += String(m_selfTest[3], 1);
            m_tmp += "gy";
            m_tmp += String(m_selfTest[4], 1);
            m_tmp += "gz";
            m_tmp += String(m_selfTest[5], 1);
            Oled::Singleton->push(m_tmp);
            delay(5000);
            return SELF_TEST_FAILED_ERROR;
        }

        if (SerialDebug) {
            Serial.print("x-axis self test: acceleration trim within : ");
            Serial.print(m_selfTest[0], 1);
            Serial.println("% of factory value");
            Serial.print("y-axis self test: acceleration trim within : ");
            Serial.print(m_selfTest[1], 1);
            Serial.println("% of factory value");
            Serial.print("z-axis self test: acceleration trim within : ");
            Serial.print(m_selfTest[2], 1);
            Serial.println("% of factory value");
            Serial.print("x-axis self test: gyration trim within : ");
            Serial.print(m_selfTest[3], 1);
            Serial.println("% of factory value");
            Serial.print("y-axis self test: gyration trim within : ");
            Serial.print(m_selfTest[4], 1);
            Serial.println("% of factory value");
            Serial.print("z-axis self test: gyration trim within : ");
            Serial.print(m_selfTest[5], 1);
            Serial.println("% of factory value");
        }

        m_tmp = "|";
        m_tmp += m_label;
        m_tmp += " Resolutions";
        Oled::Singleton->push(m_tmp);
        // get sensor resolutions, only need to do this once
        getAres();
        getGres();
        getMres();

        m_tmp = "|";
        m_tmp += m_label;
        m_tmp += " ac";
        m_tmp += 0x2 << m_aScale;
        m_tmp += " gy";
        m_tmp += (m_gScale + 1) * 250;
        Oled::Singleton->push(m_tmp);


        if (SerialDebug) {
            Serial.println(" Calibrate gyro and accel");
            // Calibrate gyro and accelerometers, load biases in bias registers
            accelgyrocalMPU9250(m_gyroBias, m_accelBias);
            Serial.println("accel biases (mg)");
            Serial.println(1000.0 * m_accelBias[0]);
            Serial.println(1000.0 * m_accelBias[1]);
            Serial.println(1000.0 * m_accelBias[2]);
            Serial.println("gyro biases (dps)");
            Serial.println(m_gyroBias[0]);
            Serial.println(m_gyroBias[1]);
            Serial.println(m_gyroBias[2]);
        }

        if (UseDisplay) {
            m_tmp = "|acBi x";
            m_tmp += (int) (1000 * m_accelBias[0]);
            m_tmp += " y";
            m_tmp += (int) (1000 * m_accelBias[1]);
            m_tmp += " z";
            m_tmp += (int) (1000 * m_accelBias[2]);
            Oled::Singleton->push(m_tmp);
            m_tmp = "|gyBi x";
            m_tmp += String(m_gyroBias[0], 1);
            m_tmp += " y";
            m_tmp += String(m_gyroBias[1], 1);
            m_tmp += " z";
            m_tmp += String(m_gyroBias[2], 1);
            Oled::Singleton->push(m_tmp);
        }

        m_tmp = "|";
        m_tmp += m_label;
        m_tmp += " init";
        Oled::Singleton->push(m_tmp);

        initMPU9250();
        // Initialize device for active mode read of acclerometer, gyroscope, and temperature
        Serial.println("MPU9250 initialized for active data mode....");

        /*
         * Initialize AK8963 magnetometer
         *
         */

        // Read the WHO_AM_I register of the magnetometer, this is a good test of communication
        uint8_t d = readByte(m_bus, AK8963_ADDRESS, AK8963_WHO_AM_I);  // Read WHO_AM_I register for AK8963
        if (SerialDebug) {
            Serial.print("AK8963 ");
            Serial.print("I AM ");
            Serial.print(d, HEX);
            Serial.print(" I should be ");
            Serial.println(0x48, HEX);
        }

        if (d == 0x48) {
            m_tmp = "|";
            m_tmp += m_label;
            m_tmp += " AK8963 @";
            m_tmp += String(AK8963_ADDRESS, HEX);
            m_tmp += " OK";
            Oled::Singleton->push(m_tmp);

            m_tmp = "|-> calibrating";
            Oled::Singleton->push(m_tmp);

            // Get magnetometer calibration from AK8963 ROM
            initAK8963(m_magCalibration);

            magcalMPU9250(m_magBias, m_magScale);
            if (SerialDebug) {
                Serial.println("AK8963 mag biases (mG)");
                Serial.println(m_magBias[0]);
                Serial.println(m_magBias[1]);
                Serial.println(m_magBias[2]);
                Serial.println("AK8963 mag scale (mG)");
                Serial.println(m_magScale[0]);
                Serial.println(m_magScale[1]);
                Serial.println(m_magScale[2]);
                Serial.println("Calibration values: ");
                Serial.print("X-Axis sensitivity adjustment value ");
                Serial.println(m_magCalibration[0], 2);
                Serial.print("Y-Axis sensitivity adjustment value ");
                Serial.println(m_magCalibration[1], 2);
                Serial.print("Z-Axis sensitivity adjustment value ");
                Serial.println(m_magCalibration[2], 2);
            }
            if (UseDisplay) {
                m_tmp = "|";
                m_tmp += m_label;
                m_tmp += " x";
                m_tmp += String(m_magCalibration[0], 2);
                m_tmp += " y";
                m_tmp += String(m_magCalibration[1], 2);
                m_tmp += " z";
                m_tmp += String(m_magCalibration[2], 2);
                Oled::Singleton->push(m_tmp);
            }
            // Initialize device for active mode read of magnetometer
            Serial.println("AK8963 initialized for active data mode....");
            if (UseDisplay) {
                Oled::Singleton->push("|active data mode");
            }

        }
        else {
            m_tmp = "|";
            m_tmp += m_label;
            m_tmp += " AK8963 @";
            m_tmp += String(AK8963_ADDRESS, HEX);
            m_tmp += " NOT OK";
            Oled::Singleton->push(m_tmp);
            delay(5000);
            return NO_COMMUNICATION_ERROR;
        }

        /*
         * Reset the MS5637 pressure sensor
         *
         */

        m_tmp = "|";
        m_tmp += m_label;
        m_tmp += " MS5637 ";
        MS5637Reset(m_bus);
        delay(100); // wait for it

        if (SerialDebug) Serial.println("MS5637 pressure sensor reset...");

        // Read PROM data from MS5637 pressure sensor
        MS5637PromRead(m_bus, m_pCal);
        m_nCrc = MS5637checkCRC(m_pCal);  //calculate checksum to ensure integrity of MS5637 calibration data
        uint8_t refCRC = m_pCal[0] >> 12;

        if (m_nCrc == refCRC) {
            m_tmp += "OK";
            Oled::Singleton->push(m_tmp);
        }
        else {
            m_tmp += "NOT OK";
            Oled::Singleton->push(m_tmp);

            if (SerialDebug) {
                Serial.println("PROM dta read:");
                Serial.print("C0 = ");
                Serial.println(m_pCal[0]);
                Serial.print("C1 = ");
                Serial.println(m_pCal[1]);
                Serial.print("C2 = ");
                Serial.println(m_pCal[2]);
                Serial.print("C3 = ");
                Serial.println(m_pCal[3]);
                Serial.print("C4 = ");
                Serial.println(m_pCal[4]);
                Serial.print("C5 = ");
                Serial.println(m_pCal[5]);
                Serial.print("C6 = ");
                Serial.println(m_pCal[6]);
                Serial.print("Checksum = ");
                Serial.print(m_nCrc);
                Serial.print(" , should be ");
                Serial.println(refCRC);
            }

            delay(5000);
            return SELF_TEST_FAILED_ERROR;
        }

        /*
         * All sensors initialized, attach interrupt, and done.
         *
         */

        // define interrupt for INT pin output of MPU9250
        // INTERRUPTS ARE NOT USED!!!
        // attachInterrupt(m_intPin, isr, RISING);
        // m_tmp = "|";
        // m_tmp += m_label;
        // m_tmp += " INT ";
        // m_tmp += String(m_intPin);
        // Oled::Singleton->push(m_tmp);
        // delay(1000);
    }
    else {
        Serial.print("Could not connect to MPU9250: 0x");
        Serial.println(c, HEX);

        m_tmp = "|";
        m_tmp += m_label;
        m_tmp += " ";
        m_tmp += String(m_addr, HEX);
        m_tmp += " NOT OK!";
        Oled::Singleton->push(m_tmp);
        delay(5000);
        return NO_COMMUNICATION_ERROR;
    }

    //
    return SUCCESS;
}

void AirImu::hook()
{
    // If intPin goes high, all data registers have new data
    // INTERRUPTS ARE NOT USED!!!


    if (m_newData) { // On interrupt, read data

        m_newData = false;  // reset newData flag

        m_filterDeltaTime = (micros() - m_filterLastUpdate); // / 1000000.0f;
        m_filterDeltaSum += m_filterDeltaTime;
        m_filterLastUpdate = micros();

        readMPU9250Data(m_mpu9250Data); // INT cleared on any read
        //   readAccelData(accelCount);  // Read the x/y/z adc values

        // Now we'll calculate the accleration value into actual g's
        // get actual g value, this depends on scale being set
        m_ax = m_mpu9250Data[0] * m_aRes - m_accelBias[0];
        m_ay = m_mpu9250Data[1] * m_aRes - m_accelBias[1];
        m_az = m_mpu9250Data[2] * m_aRes - m_accelBias[2];

        //   readGyroData(gyroCount);  // Read the x/y/z adc values

        // Calculate the gyro value into actual degrees per second
        // get actual gyro value, this depends on scale being set
        m_gx = m_mpu9250Data[4] * m_gRes;
        m_gy = m_mpu9250Data[5] * m_gRes;
        m_gz = m_mpu9250Data[6] * m_gRes;

        readMagData(m_magCount);  // Read the x/y/z adc values

        // Calculate the magnetometer values in milliGauss
        // Include factory calibration per data sheet and user environmental corrections
        if (m_newMagData) {
            // reset newMagData flag
            m_newMagData = false;
            // get actual magnetometer value, this depends on scale being set
            m_mx = (float) m_magCount[0] * m_mRes * m_magCalibration[0] - m_magBias[0];
            m_my = (float) m_magCount[1] * m_mRes * m_magCalibration[1] - m_magBias[1];
            m_mz = (float) m_magCount[2] * m_mRes * m_magCalibration[2] - m_magBias[2];
            m_mx *= m_magScale[0];
            m_my *= m_magScale[1];
            m_mz *= m_magScale[2];
        }

        // set integration time by time elapsed since last filter update


        MadgwickQuaternionUpdate(-m_ax, m_ay, m_az,
                                 m_gx * PI / 180.0f, -m_gy * PI / 180.0f, -m_gz * PI / 180.0f,
                                 m_my, -m_mx, m_mz);
//        MahonyQuaternionUpdate(
//            m_ax, m_ay, m_az,
//            m_gx * (float) DEG_TO_RAD, m_gy * (float) DEG_TO_RAD, m_gz * (float) DEG_TO_RAD,
//            m_my, m_mx, m_mz);

        qBufferCursor = (qBufferCursor + 1) % QBUFFER_SIZE;
        qBuffer[qBufferCursor] = Quaternion{m_q[0], m_q[1], m_q[2], m_q[3]};
        // Serial print and/or display at 0.5 s rate independent of data rates

        sample();

        // sum for averaging filter update rate
        m_filterElapsed += micros() - m_filterLastUpdate;
        m_filterCount++;

        if (micros() - m_filterLastReport > DIAG_UPDATE_RATE) {
            m_filterLastReport = micros();
            m_filterRateActual = ((float) m_filterCount / (float) m_filterDeltaSum) * ((
                MEGA_F / DIAG_UPDATE_RATE) * MEGA_F); // Hz
            m_filterRateMax = ((float) m_filterCount / (float) m_filterElapsed)
                * ((MEGA_F / DIAG_UPDATE_RATE) * MEGA_F); // Hz ((  / 1000000.0f
            m_filterCount = 0;
            m_filterElapsed = 0;
            m_filterDeltaSum = 0;
        }

    }
}

void AirImu::sample()
{
    /****************************************************************************
        Make final calculations only on request
     ****************************************************************************/

    m_sampleDeltaTime = (micros() - m_sampleLastUpdate) / 1000000.0f;
    m_sampleDeltaSum += m_sampleDeltaTime;
    m_sampleLastUpdate = micros();
    m_sampleCount++;

    /*
    m_tempCount = readTempData();  // Read the gyro adc values
    m_chip_temperature = m_tempCount / 333.87f + 21.0f; // Gyro chip temperature in degrees Centigrade

    m_d1 = MS5637Read(m_bus, ADC_D1, m_osr);  // get raw pressure value
    m_d2 = MS5637Read(m_bus, ADC_D2, m_osr);  // get raw temperature value
    m_dt = m_d2 - m_pCal[5] * (float) (2 << 8); // calculate temperature difference from reference
    m_offset = m_pCal[2] * (float) (1 << 17) + m_dt * m_pCal[4] / (float) (1 << 6);
    m_sens = m_pCal[1] * (float) (1 << 16) + m_dt * m_pCal[3] / (float) (2 << 7);

    // First-order Temperature in degrees Centigrade
    m_temperature = (2000.0f + (m_dt * m_pCal[6]) / (float) (2 << 23)) / 100.0f;

    // Second order corrections
    if (m_temperature > 20.0f) {
        m_t2 = 5.0f * m_dt * m_dt / (float) (float) pow(2., 38.); // correction for high temperatures
        m_offset2 = 0;
        m_sens2 = 0;
    }
    if (m_temperature < 20.0f)                  // correction for low temperature
    {
        m_t2 = 3.0f * m_dt * m_dt / (float) pow(2., 33.);
        m_offset2 = 61.0f * (100.0f * m_temperature - 2000.0f) * (100.0f * m_temperature - 2000.0f) / 16.0f;
        m_sens2 = 29.0f * (100.0f * m_temperature - 2000.0f) * (100.0f * m_temperature - 2000.0f) / 16.0f;
    }
    if (m_temperature < -15.0f)                     // correction for very low temperature
    {
        m_offset2 += 17.0f * (100.0f * m_temperature + 1500.0f) * (100.0f * m_temperature + 1500.0f);
        m_sens2 += 9.0f * (100.0f * m_temperature + 1500.0f) * (100.0f * m_temperature + 1500.0f);
    }
    // End of second order corrections
    //
    m_temperature = m_temperature - m_t2 / 100.0f;
    m_offset = m_offset - m_offset2;
    m_sens = m_sens - m_sens2;

    // Pressure in mbar or kPa
    m_pressure = (((m_d1 * m_sens) / (float) (1 << 21) - m_offset) / (float) (2 << 15)) / 100.0f;

    // Accurate for the roof on my house; convert from feet to meters
    const int32_t station_elevation_m = (int32_t) (1050.0f * 0.3048f);

    // pressure is now in millibars
    float baroin = m_pressure;

    // Formula to correct absolute pressure in millbars to "altimeter pressure" in inches of mercury
    // comparable to weather report pressure
    float part1 = baroin - 0.3f; //Part 1 of formula
    const float part2 = 0.0000842288f;
    float part3 = (float) pow(part1, 0.190284f);
    float part4 = (float) station_elevation_m / part3;
    float part5 = (1.0f + (part2 * part4));
    float part6 = (float) pow(part5, 5.2553026f);
    float altimeter_setting_pressure_mb = part1 * part6; // Output is now in adjusted millibars
    baroin = altimeter_setting_pressure_mb * 0.02953f;

    m_altitude = 145366.45f * (1.0f - (float) pow((m_pressure / 1013.25f), 0.190284f));
    */

    // Define output variables from updated quaternion---these are Tait-Bryan angles, commonly used in aircraft orientation.
    // In this coordinate system, the positive z-axis is down toward Earth.
    // Yaw is the angle between Sensor x-axis and Earth magnetic North (or true North if corrected for local declination, looking down on the sensor positive yaw is counterclockwise.
    // Pitch is angle between sensor x-axis and Earth ground plane, toward the Earth is positive, up toward the sky is negative.
    // Roll is angle between sensor y-axis and Earth ground plane, y-axis up is positive roll.
    // These arise from the definition of the homogeneous rotation matrix constructed from quaternions.
    // Tait-Bryan angles as well as Euler angles are non-commutative; that is, the get the correct orientation the rotations must be
    // applied in the correct order which for this configuration is yaw, pitch, and then roll.
    // For more see http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles which has additional links.
    //Software AHRS:
    //   yaw   = atan2f(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);
    //   pitch = -asinf(2.0f * (q[1] * q[3] - q[0] * q[2]));
    //   roll  = atan2f(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
    //   pitch *= 180.0f / PI;
    //   yaw   *= 180.0f / PI;
    //   yaw   += 13.8f; // Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
    //   if(yaw < 0) yaw   += 360.0f; // Ensure yaw stays between 0 and 360
    //   roll  *= 180.0f / PI;
    m_a12 = 2.0f * (m_q[1] * m_q[2] + m_q[0] * m_q[3]);
    m_a22 = m_q[0] * m_q[0] + m_q[1] * m_q[1] - m_q[2] * m_q[2] - m_q[3] * m_q[3];
    m_a31 = 2.0f * (m_q[0] * m_q[1] + m_q[2] * m_q[3]);
    m_a32 = 2.0f * (m_q[1] * m_q[3] - m_q[0] * m_q[2]);
    m_a33 = m_q[0] * m_q[0] - m_q[1] * m_q[1] - m_q[2] * m_q[2] + m_q[3] * m_q[3];
    m_pitch = -asinf(m_a32);
    m_roll = atan2f(m_a31, m_a33);
    m_yaw = atan2f(m_a12, m_a22);
    m_pitch *= 180.0f / PI;
    m_yaw *= 180.0f / PI;
    // Declination at Danville, California is 13 degrees 48 minutes and 47 seconds on 2014-04-04
    m_yaw += 13.8f;
    // Ensure yaw stays between 0 and 360
    if (m_yaw < 0) m_yaw += 360.0f;
    m_pitch_norm = m_pitch / 360.0f;
    m_roll_norm = m_roll / 360.0f;
    m_yaw_norm = m_yaw / 360.0f;
    m_roll *= 180.0f / PI;

    // rotation matrix for the current quaternion
//    float m_qrot_mtrx[3][3] = {
//        {
//            1.0f - 2 * m_q[2] * m_q[2] - 2.0f * m_q[3] * m_q[3],
//            2.0f * (m_q[1] * m_q[2] + m_q[0] * m_q[3]),
//            2.0f * (m_q[1] * m_q[3] - m_q[0] * m_q[2])
//        },
//        {
//            2.0f * (m_q[1] * m_q[2] - m_q[0] * m_q[3]),
//            1.0f - 2.0f * m_q[1] * m_q[1] - 2.0f * m_q[3] * m_q[3],
//            2.0f * (m_q[2] * m_q[3] + m_q[0] * m_q[1])
//        },
//        {
//            2.0f * (m_q[1] * m_q[3] + m_q[0] * m_q[2]),
//            2.0f * (m_q[2] * m_q[3] - m_q[0] * m_q[1]),
//            1.0f - 2.0f * m_q[1] * m_q[1] - 2.0f * m_q[2] * m_q[2]
//        }
//    };

    m_lin_ax = m_ax + m_a32; // this needed to be different from the reference implementation for some reason...
    m_lin_ay = m_ay - m_a31;
    m_lin_az = m_az - m_a33;

    m_lin_ax_norm = m_lin_ax * m_aRes * 4096.0f;
    m_lin_ay_norm = m_lin_ay * m_aRes * 4096.0f;
    m_lin_az_norm = m_lin_az * m_aRes * 4096.0f;
    m_gx_norm = m_gx * m_gRes / 360.0f * 128.0f;
    m_gy_norm = m_gy * m_gRes / 360.0f * 128.0f;
    m_gz_norm = m_gz * m_gRes / 360.0f * 128.0f;
    m_mx_norm = m_mx * m_mRes;
    m_my_norm = m_my * m_mRes;
    m_mz_norm = m_mz * m_mRes;
    m_gyr_norm = sqrtf(m_gx_norm * m_gx_norm + m_gy_norm * m_gy_norm + m_gz_norm * m_gz_norm);
    m_mag_norm = sqrtf(m_mx_norm * m_mx_norm + m_my_norm * m_my_norm + m_mz_norm * m_mz_norm);
    m_acc_norm = sqrtf(m_lin_ax_norm * m_lin_ax_norm + m_lin_ay_norm * m_lin_ay_norm + m_lin_az_norm * m_lin_az_norm);

    //m_qAvg = getAvg(qBuffer, qBufferCursor, QBUFFER_SIZE / 2);

    m_sampleElapsed += micros() - m_sampleLastUpdate;

    if (m_sampleCount % 200 == 0) {
        m_sampleRateMax = (float) m_sampleCount / ((float) m_sampleElapsed / 1000000.0f); // Hz
        m_sampleRateActual = (float) m_sampleCount / m_sampleDeltaSum; // Hz
        m_sampleCount = 0;
        m_sampleElapsed = 0;
        m_sampleDeltaSum = 0;
    }

}

void AirImu::viewHook()
{
    if (micros() - AirImu::m_viewLastUpdate > DISPLAY_UPDATE_RATE) {
        AirImu::m_viewLastUpdate = micros();

        Oled::Singleton->setCursor(0, 0);
        Oled::Singleton->println("                    ");
        Oled::Singleton->println("                    ");

        int16_t line = 0;
        Oled::Singleton->setCursor(0, (int16_t) line);
        Oled::Singleton->print((AirImu::Sensor0->m_ax), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor0->m_ay), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor0->m_az), 2);
        Oled::Singleton->print("ac");
        line += 8;
        Oled::Singleton->setCursor(0, (int16_t) line);
        Oled::Singleton->print((AirImu::Sensor0->m_q[0]), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor0->m_q[1]), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor0->m_q[2]), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor0->m_q[3]), 2);
        Oled::Singleton->print("q");
        line += 8;
        Oled::Singleton->setCursor(0, (int16_t) line);
        Oled::Singleton->print((AirImu::Sensor1->m_ax), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor1->m_ay), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor1->m_az), 2);
        Oled::Singleton->print("ac");
        line += 8;
        Oled::Singleton->setCursor(0, (int16_t) line);
        Oled::Singleton->print((AirImu::Sensor1->m_q[0]), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor1->m_q[1]), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor1->m_q[2]), 2);
        Oled::Singleton->print(" ");
        Oled::Singleton->print((AirImu::Sensor1->m_q[3]), 2);
        Oled::Singleton->print("q");
        line += 8;
        Oled::Singleton->setCursor(0, (int16_t) line);
        Oled::Singleton->print(AirImu::Sensor0->m_filterRateActual, 0);
        Oled::Singleton->print("/");
        Oled::Singleton->print(AirImu::Sensor0->m_filterRateMax, 0);
        Oled::Singleton->print("Hz ");
        Oled::Singleton->print(AirImu::Sensor1->m_filterRateActual, 0);
        Oled::Singleton->print("/");
        Oled::Singleton->print(AirImu::Sensor1->m_filterRateMax, 0);
        Oled::Singleton->print("Hz");
        Oled::Singleton->setCursor(64, (int16_t) line);
    }
}

//===================================================================================================================
//====== Set of useful function to access acceleration. gyroscope, magnetometer, and temperature data
//===================================================================================================================

void AirImu::getMres()
{
    switch (m_mScale) {
        // Possible magnetometer scales (and their register bit settings) are:
        // 14 bit resolution (0) and 16 bit resolution (1)
        case MFS_14BITS:
            m_mRes = 10.0f * 4912.0f / 8190.0f; // Proper scale to return milliGauss
            break;
        case MFS_16BITS:
            m_mRes = 10.0f * 4912.0f / 32760.0f; // Proper scale to return milliGauss
            break;
        default:
            break;
    }
}

void AirImu::getGres()
{
    switch (m_gScale) {
        // Possible gyro scales (and their register bit settings) are:
        // 250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS  (11).
        // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
        case GFS_250DPS:
            m_gRes = 250.0f / 32768.0f;
            break;
        case GFS_500DPS:
            m_gRes = 500.0f / 32768.0f;
            break;
        case GFS_1000DPS:
            m_gRes = 1000.0f / 32768.0f;
            break;
        case GFS_2000DPS:
            m_gRes = 2000.0f / 32768.0f;
            break;
        default:
            break;
    }
}

void AirImu::getAres()
{
    switch (m_aScale) {
        // Possible accelerometer scales (and their register bit settings) are:
        // 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
        // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
        case AFS_2G:
            m_aRes = 2.0f / 32768.0f;
            break;
        case AFS_4G:
            m_aRes = 4.0f / 32768.0f;
            break;
        case AFS_8G:
            m_aRes = 8.0f / 32768.0f;
            break;
        case AFS_16G:
            m_aRes = 16.0f / 32768.0f;
            break;
        default:
            break;
    }
}

void AirImu::readMPU9250Data(int16_t *destination)
{
    uint8_t rawData[14];  // x/y/z accel register data stored here
    readBytes(m_bus, m_addr, ACCEL_XOUT_H, 14, &rawData[0]);  // Read the 14 raw data registers into data array
    destination[0] = ((int16_t) rawData[0] << 8) | rawData[1];  // Turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t) rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t) rawData[4] << 8) | rawData[5];
    destination[3] = ((int16_t) rawData[6] << 8) | rawData[7];
    destination[4] = ((int16_t) rawData[8] << 8) | rawData[9];
    destination[5] = ((int16_t) rawData[10] << 8) | rawData[11];
    destination[6] = ((int16_t) rawData[12] << 8) | rawData[13];
}

void AirImu::readAccelData(int16_t *destination)
{
    uint8_t rawData[6];  // x/y/z accel register data stored here
    readBytes(m_bus, m_addr, ACCEL_XOUT_H, 6, &rawData[0]);  // Read the six raw data registers into data array
    destination[0] = ((int16_t) rawData[0] << 8) | rawData[1];  // Turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t) rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t) rawData[4] << 8) | rawData[5];
}

void AirImu::readGyroData(int16_t *destination)
{
    uint8_t rawData[6];  // x/y/z gyro register data stored here
    readBytes(m_bus,
              m_addr,
              GYRO_XOUT_H,
              6,
              &rawData[0]);  // Read the six raw data registers sequentially into data array
    destination[0] = ((int16_t) rawData[0] << 8) | rawData[1];  // Turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t) rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t) rawData[4] << 8) | rawData[5];
}

void AirImu::readMagData(int16_t *destination)
{
    uint8_t
        rawData[7];  // x/y/z gyro register data, ST2 register stored here, must read ST2 at end of data acquisition
    m_newMagData = (bool) (readByte(m_bus, AK8963_ADDRESS, AK8963_ST1) & 0x01);
    if (m_newMagData) { // wait for magnetometer data ready bit to be set
        readBytes(m_bus,
                  AK8963_ADDRESS,
                  AK8963_XOUT_L,
                  7,
                  &rawData[0]);  // Read the six raw data and ST2 registers sequentially into data array
        uint8_t c = rawData[6]; // End data read by reading ST2 register
        if (!(c & 0x08)) { // Check if magnetic sensor overflow set, if not then report data
            destination[0] =
                ((int16_t) rawData[1] << 8) | rawData[0];  // Turn the MSB and LSB into a signed 16-bit value
            destination[1] = ((int16_t) rawData[3] << 8) | rawData[2];  // Data stored as little Endian
            destination[2] = ((int16_t) rawData[5] << 8) | rawData[4];
        }
    }
}

int16_t AirImu::readTempData()
{
    // x/y/z gyro register data stored here
    uint8_t rawData[2];
    // Read the two raw data registers sequentially into data array
    readBytes(m_bus, m_addr, TEMP_OUT_H, 2, &rawData[0]);
    // Turn the MSB and LSB into a 16-bit value
    return ((int16_t) rawData[0] << 8) | rawData[1];
}

void AirImu::initAK8963(float *destination)
{
    // First extract the factory calibration for each magnetometer axis
    // x/y/z gyro calibration data stored here
    uint8_t rawData[3];
    // Power down magnetometer
    writeByte(m_bus, AK8963_ADDRESS, AK8963_CNTL, 0x00);
    delay(10);
    // Enter Fuse ROM access mode
    writeByte(m_bus, AK8963_ADDRESS, AK8963_CNTL, 0x0F);
    delay(10);
    // Read the x-, y-, and z-axis calibration values
    readBytes(m_bus, AK8963_ADDRESS, AK8963_ASAX, 3, &rawData[0]);
    // Return x-axis sensitivity adjustment values, etc.
    destination[0] = (float) ((rawData[0] - 128.0f) / 256.0f + 1.0f);
    destination[1] = (float) ((rawData[1] - 128.0f) / 256.0f + 1.0f);
    destination[2] = (float) ((rawData[2] - 128.0f) / 256.0f + 1.0f);
    // Power down magnetometer
    writeByte(m_bus, AK8963_ADDRESS, AK8963_CNTL, 0x00);
    delay(10);

    // Configure the magnetometer for continuous read and highest resolution
    // set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
    // and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates

    // Set magnetometer data resolution and sample ODR
    writeByte(m_bus, AK8963_ADDRESS, AK8963_CNTL, m_mScale << 4 | m_mMode);
    delay(10);
}

void AirImu::initMPU9250()
{
    // wake up device
    // Clear sleep mode bit (6), enable all sensors
    writeByte(m_bus, m_addr, PWR_MGMT_1, 0x00);
    delay(200); // Wait for all registers to reset

    // get stable time source
    // Auto select clock source to be PLL gyroscope reference if ready else
    writeByte(m_bus, m_addr, PWR_MGMT_1, 0x01);
    delay(200);

    // Configure Gyro and Thermometer
    // Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively;
    // minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
    // be higher than 1 / 0.0059 = 170 Hz
    // DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
    // With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
    writeByte(m_bus, m_addr, CONFIG, 0x03);

    // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
    // Use a 200 Hz rate; a rate consistent with the filter update rate
    writeByte(m_bus, m_addr, SMPLRT_DIV, 0x04);
    // determined inset in CONFIG above

    // Set gyroscope full scale range
    // Range selects FS_SEL and GFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
    // get current GYRO_CONFIG register value
    uint8_t c = readByte(m_bus, m_addr, GYRO_CONFIG);
    // c = c & ~0xE0; // Clear self-test bits [7:5]
    // Clear Fchoice bits [1:0]
    c = (uint8_t) (c & ~0x03);
    // Clear GFS bits [4:3]
    c = (uint8_t) (c & ~0x18);
    // Set full scale range for the gyro
    c = c | m_gScale << 3;
    // Set Fchoice for the gyro to 11 by writing its inverse to bits 1:0 of GYRO_CONFIG
    // c =| 0x00;
    writeByte(m_bus, m_addr, GYRO_CONFIG, c); // Write new GYRO_CONFIG value to register

    // Set accelerometer full-scale range configuration
    c = readByte(m_bus, m_addr, ACCEL_CONFIG); // get current ACCEL_CONFIG register value
    // c = c & ~0xE0; // Clear self-test bits [7:5]
    c = (c & ~0x18);  // Clear AFS bits [4:3]
    c = c | m_aScale << 3; // Set full scale range for the accelerometer
    writeByte(m_bus, m_addr, ACCEL_CONFIG, c); // Write new ACCEL_CONFIG register value

    // Set accelerometer sample rate configuration
    // It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
    // accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
    c = readByte(m_bus, m_addr, ACCEL_CONFIG2); // get current ACCEL_CONFIG2 register value
    c = (c & ~0x0F); // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
    c = (c | 0x03);  // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
    writeByte(m_bus, m_addr, ACCEL_CONFIG2, c); // Write new ACCEL_CONFIG2 register value

    // The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
    // but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

    // Configure Interrupts and Bypass Enable
    // Set interrupt pin active high, log-pull, hold interrupt pin level HIGH until interrupt cleared,
    // clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips
    // can join the I2C bus and all can be controlled by the Arduino as master
    //writeByte(m_bus, m_addr, INT_PIN_CFG, 0x62); // INT is held,  clear by any read, open drain
    //writeByte(m_bus, m_addr, INT_PIN_CFG, 0x52); // INT is pulse, clear by any read, open drain
    //writeByte(m_bus, m_addr, INT_PIN_CFG, 0xA2); //
    //writeByte(m_bus, m_addr, INT_PIN_CFG, 0x32); // INT is held,  clear by any read
    //writeByte(m_bus, m_addr, INT_PIN_CFG, 0x22); // INT is held,  clear by INT_STATUS
    // INT is pulse, clear by any read
    writeByte(m_bus, m_addr, INT_PIN_CFG, 0x12);
    // Enable data ready (bit 0) interrupt
    writeByte(m_bus, m_addr, INT_ENABLE, 0x01);

    delay(100);
}

// Function which accumulates gyro and accelerometer data after device initialization. It calculates the average
// of the at-rest readings and then loads the resulting offsets into accelerometer and gyro bias registers.
void AirImu::accelgyrocalMPU9250(float *dest1, float *dest2)
{
    // data array to hold accelerometer and gyro x, y, z, data
    uint8_t data[12];

    uint16_t ii, packet_count, fifo_count;
    int32_t gyro_bias[3] = {0, 0, 0}, accel_bias[3] = {0, 0, 0};

    // reset device
    // Write a one to bit 7 reset bit; toggle reset device
    writeByte(m_bus, m_addr, PWR_MGMT_1, 0x80);
    delay(100);

    // get stable time source; Auto select clock source to be PLL gyroscope reference if ready
    // else use the internal oscillator, bits 2:0 = 001
    writeByte(m_bus, m_addr, PWR_MGMT_1, 0x01);
    writeByte(m_bus, m_addr, PWR_MGMT_2, 0x00);
    delay(200);

    // Configure device for bias calculation
    writeByte(m_bus, m_addr, INT_ENABLE, 0x00);   // Disable all interrupts
    writeByte(m_bus, m_addr, FIFO_EN, 0x00);      // Disable FIFO
    writeByte(m_bus, m_addr, PWR_MGMT_1, 0x00);   // Turn on internal clock source
    writeByte(m_bus, m_addr, I2C_MST_CTRL, 0x00); // Disable I2C master
    writeByte(m_bus, m_addr, USER_CTRL, 0x00);    // Disable FIFO and I2C master modes
    writeByte(m_bus, m_addr, USER_CTRL, 0x0C);    // Reset FIFO and DMP
    delay(15);

    // Configure MPU6050 gyro and accelerometer for bias calculation
    writeByte(m_bus, m_addr, CONFIG, 0x01);       // Set low-pass filter to 188 Hz
    writeByte(m_bus, m_addr, SMPLRT_DIV, 0x00);   // Set sample rate to 1 kHz
    writeByte(m_bus, m_addr, GYRO_CONFIG, 0x00);  // Set gyro full-scale to 250 degrees per second, maximum sensitivity
    writeByte(m_bus, m_addr, ACCEL_CONFIG, 0x00); // Set accelerometer full-scale to 2 g, maximum sensitivity

    uint16_t gyrosensitivity = 131;          // = 131 LSB/degrees/sec
    uint16_t accelsensitivity = 16384;       // = 16384 LSB/g

    // Configure FIFO to capture accelerometer and gyro data for bias calculation
    // Enable FIFO
    writeByte(m_bus, m_addr, USER_CTRL, 0x40);
    // Enable gyro and accelerometer sensors for FIFO  (max size 512 bytes in MPU-9150)
    writeByte(m_bus, m_addr, FIFO_EN, 0x78);
    // accumulate 40 samples in 40 milliseconds = 480 bytes
    delay(40);

    // At end of sample accumulation, turn off FIFO sensor read
    writeByte(m_bus, m_addr, FIFO_EN, 0x00);        // Disable gyro and accelerometer sensors for FIFO
    readBytes(m_bus, m_addr, FIFO_COUNTH, 2, &data[0]); // read FIFO sample m_sampleElapsed
    fifo_count = ((uint16_t) data[0] << 8) | data[1];
    packet_count = (uint16_t) (fifo_count / 12); // How many sets of full gyro and accelerometer data for averaging

    for (ii = 0; ii < packet_count; ii++) {
        int16_t accel_temp[3] = {0, 0, 0}, gyro_temp[3] = {0, 0, 0};
        // read data for averaging
        readBytes(m_bus, m_addr, FIFO_R_W, 12, &data[0]);
        // Form signed 16-bit integer for each sample in FIFO
        accel_temp[0] = (int16_t) (((int16_t) data[0] << 8) | data[1]);
        accel_temp[1] = (int16_t) (((int16_t) data[2] << 8) | data[3]);
        accel_temp[2] = (int16_t) (((int16_t) data[4] << 8) | data[5]);
        gyro_temp[0] = (int16_t) (((int16_t) data[6] << 8) | data[7]);
        gyro_temp[1] = (int16_t) (((int16_t) data[8] << 8) | data[9]);
        gyro_temp[2] = (int16_t) (((int16_t) data[10] << 8) | data[11]);

        // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
        accel_bias[0] += (int32_t) accel_temp[0];
        accel_bias[1] += (int) accel_temp[1];
        accel_bias[2] += (int) accel_temp[2];
        gyro_bias[0] += (int) gyro_temp[0];
        gyro_bias[1] += (int) gyro_temp[1];
        gyro_bias[2] += (int) gyro_temp[2];

    }
    // Normalize sums to get average m_sampleElapsed biases
    accel_bias[0] /= (int) packet_count;
    accel_bias[1] /= (int) packet_count;
    accel_bias[2] /= (int) packet_count;
    gyro_bias[0] /= (int) packet_count;
    gyro_bias[1] /= (int) packet_count;
    gyro_bias[2] /= (int) packet_count;

    if (accel_bias[2] > 0L) {
        // Remove gravity from the z-axis accelerometer bias calculation
        accel_bias[2] -= (int) accelsensitivity;
    }
    else {
        accel_bias[2] += (int) accelsensitivity;
    }

    // Construct the gyro biases for log to the hardware gyro bias registers, which are reset to zero upon device startup
    // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
    data[0] = (-gyro_bias[0] / 4 >> 8) & 0xFF;
    // Biases are additive, so m_change sign on calculated average gyro biases
    data[1] = (-gyro_bias[0] / 4) & 0xFF;
    data[2] = (-gyro_bias[1] / 4 >> 8) & 0xFF;
    data[3] = (-gyro_bias[1] / 4) & 0xFF;
    data[4] = (-gyro_bias[2] / 4 >> 8) & 0xFF;
    data[5] = (-gyro_bias[2] / 4) & 0xFF;

    // push gyro biases to hardware registers
    writeByte(m_bus, m_addr, XG_OFFSET_H, data[0]);
    writeByte(m_bus, m_addr, XG_OFFSET_L, data[1]);
    writeByte(m_bus, m_addr, YG_OFFSET_H, data[2]);
    writeByte(m_bus, m_addr, YG_OFFSET_L, data[3]);
    writeByte(m_bus, m_addr, ZG_OFFSET_H, data[4]);
    writeByte(m_bus, m_addr, ZG_OFFSET_L, data[5]);

    // Output scaled gyro biases for display in the main program
    dest1[0] = (float) gyro_bias[0] / (float) gyrosensitivity;
    dest1[1] = (float) gyro_bias[1] / (float) gyrosensitivity;
    dest1[2] = (float) gyro_bias[2] / (float) gyrosensitivity;

    // Construct the accelerometer biases for log to the hardware accelerometer bias registers. These registers contain
    // factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
    // non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
    // compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
    // the accelerometer biases calculated above must be divided by 8.

    int32_t accel_bias_reg[3] = {0, 0, 0}; // A place to hold the factory accelerometer trim biases
    readBytes(m_bus, m_addr, XA_OFFSET_H, 2, &data[0]); // Read factory accelerometer trim values
    accel_bias_reg[0] = (int) (((int16_t) data[0] << 8) | data[1]);
    readBytes(m_bus, m_addr, YA_OFFSET_H, 2, &data[0]);
    accel_bias_reg[1] = (int) (((int16_t) data[0] << 8) | data[1]);
    readBytes(m_bus, m_addr, ZA_OFFSET_H, 2, &data[0]);
    accel_bias_reg[2] = (int) (((int16_t) data[0] << 8) | data[1]);

    uint32_t
        mask = 1uL; // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
    uint8_t mask_bit[3] = {0, 0, 0}; // Define array to hold mask bit for each accelerometer bias axis

    for (ii = 0; ii < 3; ii++) {
        if ((accel_bias_reg[ii] & mask))
            mask_bit[ii] = 0x01; // If temperature compensation bit is set, record that fact in mask_bit
    }

    // Construct total accelerometer bias, including calculated average accelerometer bias from above
    accel_bias_reg[0] -=
        (accel_bias[0] / 8); // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
    accel_bias_reg[1] -= (accel_bias[1] / 8);
    accel_bias_reg[2] -= (accel_bias[2] / 8);

    data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
    data[1] = (accel_bias_reg[0]) & 0xFF;
    data[1] = data[1]
        | mask_bit[0]; // preserve temperature compensation bit when writing back to accelerometer bias registers
    data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
    data[3] = (accel_bias_reg[1]) & 0xFF;
    data[3] = data[3]
        | mask_bit[1]; // preserve temperature compensation bit when writing back to accelerometer bias registers
    data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
    data[5] = (accel_bias_reg[2]) & 0xFF;
    data[5] = data[5]
        | mask_bit[2]; // preserve temperature compensation bit when writing back to accelerometer bias registers

    // Apparently this is not working for the acceleration biases in the MPU-9250
    // Are we handling the temperature correction bit properly?
    // push accelerometer biases to hardware registers
    /*  writeByte(MPU9250_ADDRESS, XA_OFFSET_H, data[0]);
        writeByte(MPU9250_ADDRESS, XA_OFFSET_L, data[1]);
        writeByte(MPU9250_ADDRESS, YA_OFFSET_H, data[2]);
        writeByte(MPU9250_ADDRESS, YA_OFFSET_L, data[3]);
        writeByte(MPU9250_ADDRESS, ZA_OFFSET_H, data[4]);
        writeByte(MPU9250_ADDRESS, ZA_OFFSET_L, data[5]);
    */
    // Output scaled accelerometer biases for display in the main program
    dest2[0] = (float) accel_bias[0] / (float) accelsensitivity;
    dest2[1] = (float) accel_bias[1] / (float) accelsensitivity;
    dest2[2] = (float) accel_bias[2] / (float) accelsensitivity;
}

void AirImu::magcalMPU9250(float *dest1, float *dest2)
{
    uint16_t ii = 0;
    uint16_t sample_count = 0;
    int32_t mag_bias[3] = {0, 0, 0};
    int32_t mag_scale[3] = {0, 0, 0};
    int16_t mag_max[3] = {-32767, -32767, -32767};
    int16_t mag_min[3] = {32767, 32767, 32767};
    int16_t mag_temp[3] = {0, 0, 0};

    Serial.println("Mag Calibration: Wave device in a figure eight until done!");
    //delay(4000); // original value
    delay(400);

    // shoot for ~fifteen seconds of mag data
    //if (Mmode == 0x02) sample_count = 128; // at 8 Hz ODR, new mag data is available every 125 ms
    //if (Mmode == 0x06) sample_count = 1500; // at 100 Hz ODR, new mag data is available every 10 ms
    // abberviated calibration
    if (m_mMode == 0x02) sample_count = 12; // at 8 Hz ODR, new mag data is available every 125 ms
    if (m_mMode == 0x06) sample_count = 150; // at 100 Hz ODR, new mag data is available every 10 ms
    for (ii = 0; ii < sample_count; ii++) {
        readMagData(mag_temp);  // Read the mag data
        for (int32_t jj = 0; jj < 3; jj++) {
            if (mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
            if (mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
        }
        if (m_mMode == 0x02) delay(135); // at 8 Hz ODR, new mag data is available every 125 ms
        if (m_mMode == 0x06) delay(12); // at 100 Hz ODR, new mag data is available every 10 ms
    }

    //    Serial.println("mag x min/max:"); Serial.println(mag_max[0]); Serial.println(mag_min[0]);
    //    Serial.println("mag y min/max:"); Serial.println(mag_max[1]); Serial.println(mag_min[1]);
    //    Serial.println("mag z min/max:"); Serial.println(mag_max[2]); Serial.println(mag_min[2]);

    // Get hard iron correction
    mag_bias[0] = (mag_max[0] + mag_min[0]) / 2; // get average x mag bias in counts
    mag_bias[1] = (mag_max[1] + mag_min[1]) / 2; // get average y mag bias in counts
    mag_bias[2] = (mag_max[2] + mag_min[2]) / 2; // get average z mag bias in counts

    dest1[0] = (float) mag_bias[0] * m_mRes * m_magCalibration[0]; // save mag biases in G for main program
    dest1[1] = (float) mag_bias[1] * m_mRes * m_magCalibration[1];
    dest1[2] = (float) mag_bias[2] * m_mRes * m_magCalibration[2];

    // Get soft iron correction estimate
    mag_scale[0] = (mag_max[0] - mag_min[0]) / 2; // get average x axis max chord length in counts
    mag_scale[1] = (mag_max[1] - mag_min[1]) / 2; // get average y axis max chord length in counts
    mag_scale[2] = (mag_max[2] - mag_min[2]) / 2; // get average z axis max chord length in counts

    float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
    avg_rad /= 3.0;

    dest2[0] = avg_rad / ((float) mag_scale[0]);
    dest2[1] = avg_rad / ((float) mag_scale[1]);
    dest2[2] = avg_rad / ((float) mag_scale[2]);

    Serial.println("Mag Calibration done!");
}

// Accelerometer and gyroscope self test; check calibration wrt factory settings
void AirImu::MPU9250SelfTest(float *destination) // Should return percent deviation from factory trim values, +/- 14 or less deviation is a pass
{
    uint8_t rawData[6] = {0, 0, 0, 0, 0, 0};
    uint8_t selfTest[6];
    int32_t gAvg[3] = {0}, aAvg[3] = {0}, aStAvg[3] = {0}, gStAvg[3] = {0};
    float factoryTrim[6];
    uint8_t FS = 0;

    writeByte(m_bus, m_addr, SMPLRT_DIV, 0x00);    // Set gyro sample rate to 1 kHz
    writeByte(m_bus, m_addr, CONFIG, 0x02);        // Set gyro sample rate to 1 kHz and DLPF to 92 Hz
    writeByte(m_bus, m_addr, GYRO_CONFIG, FS << 3); // Set full scale range for the gyro to 250 dps
    writeByte(m_bus, m_addr, ACCEL_CONFIG2, 0x02); // Set accelerometer rate to 1 kHz and bandwidth to 92 Hz
    writeByte(m_bus, m_addr, ACCEL_CONFIG, FS << 3); // Set full scale range for the accelerometer to 2 g

    for (int32_t ii = 0; ii < 200; ii++) { // get average current values of gyro and acclerometer

        // Read the six raw data registers into data array
        readBytes(m_bus, m_addr, ACCEL_XOUT_H, 6, &rawData[0]);
        // Turn the MSB and LSB into a signed 16-bit value
        aAvg[0] += ((rawData[0] << 8) | rawData[1]);
        aAvg[1] += ((rawData[2] << 8) | rawData[3]);
        aAvg[2] += ((rawData[4] << 8) | rawData[5]);

        // Read the six raw data registers sequentially into data array
        readBytes(m_bus, m_addr, GYRO_XOUT_H, 6, &rawData[0]);
        // Turn the MSB and LSB into a signed 16-bit value
        gAvg[0] += ((rawData[0] << 8) | rawData[1]);
        gAvg[1] += ((rawData[2] << 8) | rawData[3]);
        gAvg[2] += ((rawData[4] << 8) | rawData[5]);
    }

    // Get average of 200 values and store as average current readings
    for (int32_t ii = 0; ii < 3; ii++) {
        aAvg[ii] /= 200;
        gAvg[ii] /= 200;
    }

    // Configure the accelerometer for self-test
    // Enable self test on all three axes and set accelerometer range to +/- 2 g
    writeByte(m_bus, m_addr, ACCEL_CONFIG, 0xE0);
    // Enable self test on all three axes and set gyro range to +/- 250 degrees/s
    writeByte(m_bus, m_addr, GYRO_CONFIG, 0xE0);
    delay(25);  // Delay a while to let the device stabilize

    // get average self-test values of gyro and acclerometer
    for (size_t ii = 0; ii < 200; ii++) {

        // Read the six raw data registers into data array
        readBytes(m_bus, m_addr, ACCEL_XOUT_H, 6, &rawData[0]);
        // Turn the MSB and LSB into a signed 16-bit value
        aStAvg[0] += ((rawData[0] << 8) | rawData[1]);
        aStAvg[1] += ((rawData[2] << 8) | rawData[3]);
        aStAvg[2] += ((rawData[4] << 8) | rawData[5]);

        // Read the six raw data registers sequentially into data array
        readBytes(m_bus, m_addr, GYRO_XOUT_H, 6, &rawData[0]);
        // Turn the MSB and LSB into a signed 16-bit value
        gStAvg[0] += ((rawData[0] << 8) | rawData[1]);
        gStAvg[1] += ((rawData[2] << 8) | rawData[3]);
        gStAvg[2] += ((rawData[4] << 8) | rawData[5]);
    }

    // Get average of 200 values and store as average self-test readings
    for (int32_t ii = 0; ii < 3; ii++) {
        aStAvg[ii] /= 200;
        gStAvg[ii] /= 200;
    }

    // Configure the gyro and accelerometer for normal operation
    writeByte(m_bus, m_addr, ACCEL_CONFIG, 0x00);
    writeByte(m_bus, m_addr, GYRO_CONFIG, 0x00);
    // Delay a while to let the device stabilize
    delay(25);

    // Retrieve accelerometer and gyro factory Self-Test Code from USR_Reg
    selfTest[0] = readByte(m_bus, m_addr, SELF_TEST_X_ACCEL); // X-axis accel self-test results
    selfTest[1] = readByte(m_bus, m_addr, SELF_TEST_Y_ACCEL); // Y-axis accel self-test results
    selfTest[2] = readByte(m_bus, m_addr, SELF_TEST_Z_ACCEL); // Z-axis accel self-test results
    selfTest[3] = readByte(m_bus, m_addr, SELF_TEST_X_GYRO);  // X-axis gyro self-test results
    selfTest[4] = readByte(m_bus, m_addr, SELF_TEST_Y_GYRO);  // Y-axis gyro self-test results
    selfTest[5] = readByte(m_bus, m_addr, SELF_TEST_Z_GYRO);  // Z-axis gyro self-test results

    // Retrieve factory self-test value from self-test code reads
    // FT[Xa] factory trim calculation
    factoryTrim[0] = (float) (2620 / 1 << FS) * ((float) pow(1.01, ((float) selfTest[0] - 1.0)));
    // FT[Ya] factory trim calculation
    factoryTrim[1] = (float) (2620 / 1 << FS) * ((float) pow(1.01, ((float) selfTest[1] - 1.0)));
    // FT[Za] factory trim calculation
    factoryTrim[2] = (float) (2620 / 1 << FS) * ((float) pow(1.01, ((float) selfTest[2] - 1.0)));
    // FT[Xg] factory trim calculation
    factoryTrim[3] = (float) (2620 / 1 << FS) * ((float) pow(1.01, ((float) selfTest[3] - 1.0)));
    // FT[Yg] factory trim calculation
    factoryTrim[4] = (float) (2620 / 1 << FS) * ((float) pow(1.01, ((float) selfTest[4] - 1.0)));
    // FT[Zg] factory trim calculation
    factoryTrim[5] = (float) (2620 / 1 << FS) * ((float) pow(1.01, ((float) selfTest[5] - 1.0)));

    // Report results as a ratio of (STR - FT)/FT; the m_change from Factory Trim of the Self-Test Response
    // To get percent, must multiply by 100
    for (int32_t i = 0; i < 3; i++) {
        destination[i] =
            (float) (100.0 * ((float) (aStAvg[i] - aAvg[i])) / factoryTrim[i] - 100.); // Report percent differences
        destination[i + 3] = (float) (
            100.0 * ((float) (gStAvg[i] - gAvg[i])) / factoryTrim[i + 3] - 100.); // Report percent differences
    }

}

// I2C communication with the MS5637 is a little different from that with the MPU9250 and most other sensors
// For the MS5637, we write commands, and the MS5637 sends data in response, rather than directly reading
// MS5637 registers

void AirImu::MS5637Reset(i2c_t3 *bus)
{
    bus->beginTransmission(MS5637_ADDRESS);  // Initialize the Tx buffer
    bus->write(MS5637_RESET);                // Put reset command in Tx buffer
    bus->endTransmission();                  // Send the Tx buffer
}

void AirImu::MS5637PromRead(i2c_t3 *bus, uint16_t *destination)
{
    uint8_t data[2] = {0, 0};
    for (uint8_t ii = 0; ii < 7; ii++) {
        bus->beginTransmission(MS5637_ADDRESS);  // Initialize the Tx buffer
        bus->write(0xA0 | ii << 1);              // Put PROM address in Tx buffer
        bus->endTransmission(I2C_NOSTOP);        // Send the Tx buffer, but send a restart to keep connection alive
        uint8_t i = 0;
        bus->requestFrom(MS5637_ADDRESS, 2);   // Read two bytes from slave PROM address
        while (bus->available()) {
            data[i++] = bus->read();
        }               // Put read results in the Rx buffer
        destination[ii] =
            (uint16_t) (((uint16_t) data[0] << 8)
                | data[1]); // construct PROM data for return to main program
    }
}

uint32_t AirImu::MS5637Read(i2c_t3 *bus, uint8_t CMD, uint8_t OSR)  // temperature data read
{
    uint8_t data[3] = {0, 0, 0};
    bus->beginTransmission(MS5637_ADDRESS);  // Initialize the Tx buffer
    bus->write(CMD | OSR);                   // Put pressure conversion command in Tx buffer
    bus->endTransmission(I2C_NOSTOP);        // Send the Tx buffer, but send a restart to keep connection alive

    switch (OSR) {
        case ADC_256:
            delay(1);
            break;  // delay for conversion to complete
        case ADC_512:
            delay(3);
            break;
        case ADC_1024:
            delay(4);
            break;
        case ADC_2048:
            delay(6);
            break;
        case ADC_4096:
            delay(10);
            break;
        case ADC_8192:
            delay(20);
            break;
        default:
            break;
    }

    bus->beginTransmission(MS5637_ADDRESS);  // Initialize the Tx buffer
    bus->write(0x00);                        // Put ADC read command in Tx buffer
    bus->endTransmission(I2C_NOSTOP);        // Send the Tx buffer, but send a restart to keep connection alive
    uint8_t i = 0;
    bus->requestFrom(MS5637_ADDRESS, 3);     // Read three bytes from slave PROM address
    while (bus->available()) {
        data[i++] = bus->read();
    }               // Put read results in the Rx buffer
    return (uint) (((uint) data[0] << 16) | (uint) data[1] << 8
        | data[2]); // construct PROM data for return to main program
}

uint8_t AirImu::MS5637checkCRC(uint16_t *n_prom)  // calculate checksum from PROM register contents
{
    int32_t cnt;
    uint32_t n_rem = 0;
    uint8_t n_bit;

    n_prom[0] = ((n_prom[0]) & 0x0FFF);  // replace CRC byte by 0 for checksum calculation
    n_prom[7] = 0;
    for (cnt = 0; cnt < 16; cnt++) {
        if (cnt % 2 == 1) n_rem ^= (unsigned short) ((n_prom[cnt >> 1]) & 0x00FF);
        else n_rem ^= (unsigned short) (n_prom[cnt >> 1] >> 8);
        for (n_bit = 8; n_bit > 0; n_bit--) {
            if (n_rem & 0x8000) n_rem = (n_rem << 1) ^ 0x3000;
            else n_rem = (n_rem << 1);
        }
    }
    n_rem = ((n_rem >> 12) & 0x000F);
    return (n_rem ^ 0x00);
}

// I2C read/write functions for the MPU9250 and AK8963 sensors

void AirImu::writeByte(i2c_t3 *bus, uint8_t address, uint8_t subAddress, uint8_t data)
{
    bus->beginTransmission((int) address);  // Initialize the Tx buffer
    bus->write(subAddress);           // Put slave register address in Tx buffer
    bus->write(data);                 // Put data in Tx buffer
    bus->endTransmission();           // Send the Tx buffer
}

uint8_t AirImu::readByte(i2c_t3 *bus, uint8_t address, uint8_t subAddress)
{
    uint8_t data; // `data` will store the register data
    bus->beginTransmission((int) address);         // Initialize the Tx buffer
    bus->write(subAddress);                  // Put slave register address in Tx buffer
    bus->endTransmission(I2C_NOSTOP);        // Send the Tx buffer, but send a restart to keep connection alive
    //  bus.endTransmission(false);             // Send the Tx buffer, but send a restart to keep connection alive
    //  bus.requestFrom(address, 1);  // Read one byte from slave register address
    bus->requestFrom(address, (size_t) 1);   // Read one byte from slave register address
    data = bus->read();                      // Fill Rx buffer with result
    return data;                             // Return data read from slave register
}

void AirImu::readBytes(i2c_t3 *bus,
                       uint8_t address,
                       uint8_t subAddress,
                       uint8_t count,
                       uint8_t *dest)
{
    bus->beginTransmission((int) address);   // Initialize the Tx buffer
    bus->write(subAddress);            // Put slave register address in Tx buffer
    bus->endTransmission(I2C_NOSTOP);  // Send the Tx buffer, but send a restart to keep connection alive
    //  bus.endTransmission(false);       // Send the Tx buffer, but send a restart to keep connection alive
    uint8_t i = 0;
    //        bus.requestFrom(address, m_sampleElapsed);  // Read bytes from slave register address
    bus->requestFrom(address, (size_t) count);  // Read bytes from slave register address
    while (bus->available()) {
        dest[i++] = bus->read();
    }         // Put read results in the Rx buffer
}

// Implementation of Sebastian Madgwick's "...efficient orientation filter for... inertial/magnetic sensor arrays"
// (see http://www.x-io.co.uk/category/open-source/ for examples and more details)
// which fuses acceleration, rotation rate, and magnetic moments to produce a quaternion-based estimate of absolute
// device orientation -- which can be converted to yaw, pitch, and roll. Useful for stabilizing quadcopters, etc.
// The performance of the orientation filter is at least as good as conventional Kalman-based filtering algorithms
// but is much less computationally intensive---it can be performed on a 3.3 V Pro Mini operating at 8 MHz!
void AirImu::MadgwickQuaternionUpdate(float ax, float ay, float az,
                                      float gx, float gy, float gz,
                                      float mx, float my, float mz)
{
    float q1 = m_q[0], q2 = m_q[1], q3 = m_q[2], q4 = m_q[3];   // short name local variable for readability
    float norm;
    float hx, hy, _2bx, _2bz;
    float s1, s2, s3, s4;
    float qDot1, qDot2, qDot3, qDot4;

    // Auxiliary variables to avoid repeated arithmetic
    float _2q1mx;
    float _2q1my;
    float _2q1mz;
    float _2q2mx;
    float _4bx;
    float _4bz;
    float _2q1 = 2.0f * q1;
    float _2q2 = 2.0f * q2;
    float _2q3 = 2.0f * q3;
    float _2q4 = 2.0f * q4;
    float _2q1q3 = 2.0f * q1 * q3;
    float _2q3q4 = 2.0f * q3 * q4;
    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q1q4 = q1 * q4;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q2q4 = q2 * q4;
    float q3q3 = q3 * q3;
    float q3q4 = q3 * q4;
    float q4q4 = q4 * q4;

    // Normalise accelerometer measurement
    norm = sqrtf(ax * ax + ay * ay + az * az);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f / norm;
    ax *= norm;
    ay *= norm;
    az *= norm;

    // Normalise magnetometer measurement
    norm = sqrtf(mx * mx + my * my + mz * mz);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f / norm;
    mx *= norm;
    my *= norm;
    mz *= norm;

    // Reference direction of Earth's magnetic field
    _2q1mx = 2.0f * q1 * mx;
    _2q1my = 2.0f * q1 * my;
    _2q1mz = 2.0f * q1 * mz;
    _2q2mx = 2.0f * q2 * mx;
    hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
    hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
    _2bx = sqrtf(hx * hx + hy * hy);
    _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
    _4bx = 2.0f * _2bx;
    _4bz = 2.0f * _2bz;

    // Gradient decent algorithm corrective step
    s1 = -_2q3 * (2.0f * q2q4 - _2q1q3 - ax) + _2q2 * (2.0f * q1q2 + _2q3q4 - ay)
        - _2bz * q3 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx)
        + (-_2bx * q4 + _2bz * q2) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
        + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    s2 = _2q4 * (2.0f * q2q4 - _2q1q3 - ax) + _2q1 * (2.0f * q1q2 + _2q3q4 - ay)
        - 4.0f * q2 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az)
        + _2bz * q4 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx)
        + (_2bx * q3 + _2bz * q1) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
        + (_2bx * q4 - _4bz * q2) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    s3 = -_2q1 * (2.0f * q2q4 - _2q1q3 - ax) + _2q4 * (2.0f * q1q2 + _2q3q4 - ay)
        - 4.0f * q3 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az)
        + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx)
        + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
        + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    s4 = _2q2 * (2.0f * q2q4 - _2q1q3 - ax) + _2q3 * (2.0f * q1q2 + _2q3q4 - ay)
        + (-_4bx * q4 + _2bz * q2) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx)
        + (-_2bx * q1 + _2bz * q3) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
        + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
    norm = sqrtf(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
    norm = 1.0f / norm;
    s1 *= norm;
    s2 *= norm;
    s3 *= norm;
    s4 *= norm;

    // Compute rate of m_change of quaternion
    qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - m_beta * s1;
    qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - m_beta * s2;
    qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - m_beta * s3;
    qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - m_beta * s4;

    // Integrate to yield quaternion
    q1 += qDot1 * m_filterDeltaTime / MEGA_F;
    q2 += qDot2 * m_filterDeltaTime / MEGA_F;
    q3 += qDot3 * m_filterDeltaTime / MEGA_F;
    q4 += qDot4 * m_filterDeltaTime / MEGA_F;
    norm = sqrtf(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
    norm = 1.0f / norm;
    m_q[0] = q1 * norm;
    m_q[1] = q2 * norm;
    m_q[2] = q3 * norm;
    m_q[3] = q4 * norm;

}

// Similar to Madgwick scheme but uses proportional and integral filtering on the error between estimated reference vectors and
// measured ones. 
void AirImu::MahonyQuaternionUpdate(float ax, float ay, float az,
                                    float gx, float gy, float gz,
                                    float mx, float my, float mz)
{
    float q1 = m_q[0], q2 = m_q[1], q3 = m_q[2], q4 = m_q[3];   // short name local variable for readability
    float norm;
    float hx, hy, bx, bz;
    float vx, vy, vz, wx, wy, wz;
    float ex, ey, ez;
    float pa, pb, pc;

    // Auxiliary variables to avoid repeated arithmetic
    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q1q4 = q1 * q4;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q2q4 = q2 * q4;
    float q3q3 = q3 * q3;
    float q3q4 = q3 * q4;
    float q4q4 = q4 * q4;

    // Normalise accelerometer measurement
    norm = sqrtf(ax * ax + ay * ay + az * az);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f / norm;        // use reciprocal for division
    ax *= norm;
    ay *= norm;
    az *= norm;

    // Normalise magnetometer measurement
    norm = sqrtf(mx * mx + my * my + mz * mz);
    if (norm == 0.0f) return; // handle NaN
    norm = 1.0f / norm;        // use reciprocal for division
    mx *= norm;
    my *= norm;
    mz *= norm;

    // Reference direction of Earth's magnetic field
    hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
    hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
    bx = sqrtf((hx * hx) + (hy * hy));
    bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

    // Estimated direction of gravity and magnetic field
    vx = 2.0f * (q2q4 - q1q3);
    vy = 2.0f * (q1q2 + q3q4);
    vz = q1q1 - q2q2 - q3q3 + q4q4;
    wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
    wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
    wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

    // Error is cross product between estimated direction and measured direction of gravity
    ex = (ay * vz - az * vy) + (my * wz - mz * wy);
    ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
    ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
    if (m_ki > 0.0f) {
        m_eInt[0] += ex;      // accumulate integral error
        m_eInt[1] += ey;
        m_eInt[2] += ez;
    }
    else {
        m_eInt[0] = 0.0f;     // prevent integral wind up
        m_eInt[1] = 0.0f;
        m_eInt[2] = 0.0f;
    }

    // Apply feedback terms
    gx = gx + m_kp * ex + m_ki * m_eInt[0];
    gy = gy + m_kp * ey + m_ki * m_eInt[1];
    gz = gz + m_kp * ez + m_ki * m_eInt[2];

    // Integrate rate of m_change of quaternion
    pa = q2;
    pb = q3;
    pc = q4;
    q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * m_filterDeltaTime / MEGA_F);
    q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * m_filterDeltaTime / MEGA_F);
    q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * m_filterDeltaTime / MEGA_F);
    q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * m_filterDeltaTime / MEGA_F);

    // Normalise quaternion
    norm = sqrtf(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
    norm = 1.0f / norm;
    m_q[0] = q1 * norm;
    m_q[1] = q2 * norm;
    m_q[2] = q3 * norm;
    m_q[3] = q4 * norm;

}

void AirImu::imu0_jt()
{
    Sensor0->interruptHandler();
}

void AirImu::imu1_jt()
{
    Sensor1->interruptHandler();
}

void AirImu::sync()
{
    if (micros() - m_syncLastUpdate > SYNC_RATE) {
        m_syncLastUpdate = micros();
        m_newData = true;
    }
}
Quaternion AirImu::getAvg(const Quaternion pQuaternion[QBUFFER_SIZE], uint32_t cursor, const int count)
{
    Quaternion sum = {0, 0, 0, 0};
    int i = count;
    while (i > 0) {
        sum.x += pQuaternion[(cursor - i) % QBUFFER_SIZE].x;
        sum.y += pQuaternion[(cursor - i) % QBUFFER_SIZE].y;
        sum.z += pQuaternion[(cursor - i) % QBUFFER_SIZE].z;
        sum.w += pQuaternion[(cursor - i) % QBUFFER_SIZE].w;
        i--;
    }
    return Quaternion{sum.w / count, sum.x / count, sum.y / count, sum.z / count};
}
