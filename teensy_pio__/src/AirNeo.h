//
// Created by Gerold Schneider on 16.12.17.
//

#ifndef TEENSY_PIO_AIRNEO_H
#define TEENSY_PIO_AIRNEO_H

#define AIR_NEO_PIN 6
#define AIR_NEO_NUM_PIXELS 1

#include <Adafruit_NeoPixel.h>

class AirNeo
{
public:
    //Adafruit_NeoPixel *(AirNeo::Pixel);
    static Adafruit_NeoPixel *Pixel;
    static void init();
    static void hook();
    static void setup();
    AirNeo();
    static AirNeo *Singleton;
};


#endif //TEENSY_PIO_AIRNEO_H
