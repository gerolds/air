#include "AirMotor.hpp"

AirMotor::AirMotor()
{
    init();
}

void AirMotor::setup()
{
    AirMotor::Drive = new AirMotor();
    Drive->begin();
    Drive->setMode(DRV2605_MODE_INTTRIG);
    Drive->useLRA();
    Drive->selectLibrary(6);
    Drive->setWaveform(0, 64); // Transition Hum
    Drive->setWaveform(1, 11); // Double Click 60%
    Drive->setWaveform(2, 0);
    Drive->go();
}
void AirMotor::hook()
{

}
void AirMotor::init()
{
}
void AirMotor::autoCalibrate()
{
    byte fb_reg_val = 0;
    fb_reg_val |= 1 << 7; // LRA
    fb_reg_val |= 2 << 4; // brake factor 2x
    fb_reg_val |= 2 << 2; // loop gain
    byte bemf_gain = 1; // bemf gain // TODO: figure out the formula

    writeRegister8(DRV2605_REG_FEEDBACK, fb_reg_val);
}
