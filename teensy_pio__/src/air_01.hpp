//
// Created by Gerold Schneider on 06.12.17.
//

#ifndef TEENSY_PIO_AIR_01_H
#define TEENSY_PIO_AIR_01_H


#define DISPLAY_UPDATE_RATE  500000 // 15000 µs --> 66.7 Hz
#define SENSOR_UPDATE_RATE     3000 // 5000 µs --> 200 Hz
#define MESSAGE_RATE           6000 // 10000 µs --> 100 Hz
#define SERIAL_BAUD          230400
#define DIAG_UPDATE_RATE     500000
#define MEGA_F              1000000.0f
#define MEGA                1000000
#define KILO_F                 1000.0f

#include <Arduino.h>

// replacement for Wire.h, see https://github.com/nox771/i2c_t3
#include <i2c_t3.h>
#include <SPI.h>
#include <Time.h>
#include <TimeLib.h>
#include <math.h>
#include <OSCBoards.h>
#include <OSCBundle.h>
#include <OSCData.h>
#include <OSCMatch.h>
#include <OSCMessage.h>
#include <OSCTiming.h>
#include <SLIPEncodedUSBSerial.h>
#include <TeensyThreads.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <Encoder.h>

#include "Oled.hpp"
#include "AirI2c.hpp"
#include "AirSwitch.h"
#include "d_spi.h"
#include "AirEncoder.h"
#include "AirWiFi.h"
#include "AirNeo.h"
#include "AirImu.h"
#include "AirOSC.hpp"
#include "constants.hpp"
#include "util.h"
#include "test.hpp"
#include "tables.hpp"

//#include "RotaryEncoder.h"

#endif //TEENSY_PIO_AIR_01_H
