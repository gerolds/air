//
// Created by Gerold Schneider on 07.12.17.
//

#ifndef TEENSY_PIO_D_SOLENOID_H
#define TEENSY_PIO_D_SOLENOID_H

#include <Arduino.h>
#include <Adafruit_DRV2605.h>

class AirMotor: public Adafruit_DRV2605
{

public:
    void init();
    void hook();
    void setup();
    static AirMotor *Drive;
    AirMotor();
protected:
    void autoCalibrate();

};




#endif //TEENSY_PIO_D_SOLENOID_H
