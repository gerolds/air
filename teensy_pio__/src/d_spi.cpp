#define SPI_SCK_PIN 14
#include "d_spi.h"
void spi_setup()
{
    SPI.begin();
    SPI.setSCK(SPI_SCK_PIN);

    // need to re-enable pin 13 for led
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);

}


