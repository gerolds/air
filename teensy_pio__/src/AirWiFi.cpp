//
// Created by Gerold Schneider on 14.01.18.
//

#include "AirWiFi.h"
AirWiFi::AirWiFi()
{
    AirWiFi::Singleton = new AirWiFi();
    init();
}

void AirWiFi::hook()
{

}

void AirWiFi::init()
{
    WiFi.setPins(21, 27, 3, 4);
}

void AirWiFi::setup()
{
    WiFi.macAddress(mac);
    int numSSID = WiFi.scanNetworks();
    if (numSSID == -1) {
        // no networks
    }

    for (int i = 0; i < numSSID; i++) {
        Serial.print(i);
        Serial.print(") ");
        Serial.print(WiFi.SSID(i));
        Serial.print("\tSignal: ");
        Serial.print(WiFi.RSSI(i));
        Serial.print(" dBm");
        Serial.print("\tEncryption: ");
        printEncryptionType(WiFi.encryptionType(i));
        Serial.flush();
    }
}

void AirWiFi::printEncryptionType(int thisType) {
    // read the encryption type and print out the name:
    switch (thisType) {
        case ENC_TYPE_WEP:
            Serial.println("WEP");
            break;
        case ENC_TYPE_TKIP:
            Serial.println("WPA");
            break;
        case ENC_TYPE_CCMP:
            Serial.println("WPA2");
            break;
        case ENC_TYPE_NONE:
            Serial.println("None");
            break;
        case ENC_TYPE_AUTO:
            Serial.println("Auto");
            break;
        default:break;
    }
}
