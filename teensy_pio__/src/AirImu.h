#ifndef TEENSY_PIO_D_MPU9250_H
#define TEENSY_PIO_D_MPU9250_H

// See MS5637-02BA03 Low Voltage Barometric Pressure Sensor Data Sheet
#define MS5637_RESET      0x1E
#define MS5637_CONVERT_D1 0x40
#define MS5637_CONVERT_D2 0x50
#define MS5637_ADC_READ   0x00

// See also MPU-9250 Register Map and Descriptions, Revision 4.0, RM-MPU-9250A-00, Rev. 1.4, 9/9/2013 for registers not listed in
// above document; the MPU9250 and MPU9150 are virtually identical but the latter has a different register map
//
//Magnetometer Registers
#define AK8963_ADDRESS    0x0C
#define AK8963_WHO_AM_I   0x00  // should return 0x48
#define AK8963_INFO       0x01
#define AK8963_ST1        0x02  // data ready status bit 0
#define AK8963_XOUT_L     0x03  // data
#define AK8963_XOUT_H     0x04
#define AK8963_YOUT_L     0x05
#define AK8963_YOUT_H     0x06
#define AK8963_ZOUT_L     0x07
#define AK8963_ZOUT_H     0x08
#define AK8963_ST2        0x09  // Data overflow bit 3 and data read error status bit 2
#define AK8963_CNTL       0x0A  // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
#define AK8963_ASTC       0x0C  // Self test control
#define AK8963_I2CDIS     0x0F  // I2C disable
#define AK8963_ASAX       0x10  // Fuse ROM x-axis sensitivity adjustment value
#define AK8963_ASAY       0x11  // Fuse ROM y-axis sensitivity adjustment value
#define AK8963_ASAZ       0x12  // Fuse ROM z-axis sensitivity adjustment value

#define SELF_TEST_X_GYRO  0x00
#define SELF_TEST_Y_GYRO  0x01
#define SELF_TEST_Z_GYRO  0x02

#define SELF_TEST_X_ACCEL 0x0D
#define SELF_TEST_Y_ACCEL 0x0E
#define SELF_TEST_Z_ACCEL 0x0F

#define SELF_TEST_A       0x10

#define XG_OFFSET_H       0x13  // User-defined trim values for gyroscope
#define XG_OFFSET_L       0x14
#define YG_OFFSET_H       0x15
#define YG_OFFSET_L       0x16
#define ZG_OFFSET_H       0x17
#define ZG_OFFSET_L       0x18
#define SMPLRT_DIV        0x19
#define CONFIG            0x1A
#define GYRO_CONFIG       0x1B
#define ACCEL_CONFIG      0x1C
#define ACCEL_CONFIG2     0x1D
#define LP_ACCEL_ODR      0x1E
#define WOM_THR           0x1F

#define MOT_DUR           0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define ZMOT_THR          0x21  // Zero-motion detection threshold bits [7:0]
#define ZRMOT_DUR         0x22  // Duration counter threshold for zero motion interrupt generation, 16 Hz rate, LSB = 64 ms

#define FIFO_EN           0x23
#define I2C_MST_CTRL      0x24
#define I2C_SLV0_ADDR     0x25
#define I2C_SLV0_REG      0x26
#define I2C_SLV0_CTRL     0x27
#define I2C_SLV1_ADDR     0x28
#define I2C_SLV1_REG      0x29
#define I2C_SLV1_CTRL     0x2A
#define I2C_SLV2_ADDR     0x2B
#define I2C_SLV2_REG      0x2C
#define I2C_SLV2_CTRL     0x2D
#define I2C_SLV3_ADDR     0x2E
#define I2C_SLV3_REG      0x2F
#define I2C_SLV3_CTRL     0x30
#define I2C_SLV4_ADDR     0x31
#define I2C_SLV4_REG      0x32
#define I2C_SLV4_DO       0x33
#define I2C_SLV4_CTRL     0x34
#define I2C_SLV4_DI       0x35
#define I2C_MST_STATUS    0x36
#define INT_PIN_CFG       0x37
#define INT_ENABLE        0x38
#define DMP_INT_STATUS    0x39  // Check DMP interrupt
#define INT_STATUS        0x3A
#define ACCEL_XOUT_H      0x3B
#define ACCEL_XOUT_L      0x3C
#define ACCEL_YOUT_H      0x3D
#define ACCEL_YOUT_L      0x3E
#define ACCEL_ZOUT_H      0x3F
#define ACCEL_ZOUT_L      0x40
#define TEMP_OUT_H        0x41
#define TEMP_OUT_L        0x42
#define GYRO_XOUT_H       0x43
#define GYRO_XOUT_L       0x44
#define GYRO_YOUT_H       0x45
#define GYRO_YOUT_L       0x46
#define GYRO_ZOUT_H       0x47
#define GYRO_ZOUT_L       0x48
#define EXT_SENS_DATA_00  0x49
#define EXT_SENS_DATA_01  0x4A
#define EXT_SENS_DATA_02  0x4B
#define EXT_SENS_DATA_03  0x4C
#define EXT_SENS_DATA_04  0x4D
#define EXT_SENS_DATA_05  0x4E
#define EXT_SENS_DATA_06  0x4F
#define EXT_SENS_DATA_07  0x50
#define EXT_SENS_DATA_08  0x51
#define EXT_SENS_DATA_09  0x52
#define EXT_SENS_DATA_10  0x53
#define EXT_SENS_DATA_11  0x54
#define EXT_SENS_DATA_12  0x55
#define EXT_SENS_DATA_13  0x56
#define EXT_SENS_DATA_14  0x57
#define EXT_SENS_DATA_15  0x58
#define EXT_SENS_DATA_16  0x59
#define EXT_SENS_DATA_17  0x5A
#define EXT_SENS_DATA_18  0x5B
#define EXT_SENS_DATA_19  0x5C
#define EXT_SENS_DATA_20  0x5D
#define EXT_SENS_DATA_21  0x5E
#define EXT_SENS_DATA_22  0x5F
#define EXT_SENS_DATA_23  0x60
#define MOT_DETECT_STATUS 0x61
#define I2C_SLV0_DO       0x63
#define I2C_SLV1_DO       0x64
#define I2C_SLV2_DO       0x65
#define I2C_SLV3_DO       0x66
#define I2C_MST_DELAY_CTRL 0x67
#define SIGNAL_PATH_RESET 0x68
#define MOT_DETECT_CTRL   0x69
#define USER_CTRL         0x6A  // Bit 7 enable DMP, bit 3 reset DMP
#define PWR_MGMT_1        0x6B  // Device defaults to the SLEEP mode
#define PWR_MGMT_2        0x6C
#define DMP_BANK          0x6D  // Activates a specific bank in the DMP
#define DMP_RW_PNT        0x6E  // Set read/write pointer to a specific start address in specified DMP bank
#define DMP_REG           0x6F  // Register in DMP from which to read or to which to write
#define DMP_REG_1         0x70
#define DMP_REG_2         0x71
#define FIFO_COUNTH       0x72
#define FIFO_COUNTL       0x73
#define FIFO_R_W          0x74
#define WHO_AM_I_MPU9250  0x75 // Should return 0x71
#define XA_OFFSET_H       0x77
#define XA_OFFSET_L       0x78
#define YA_OFFSET_H       0x7A
#define YA_OFFSET_L       0x7B
#define ZA_OFFSET_H       0x7D
#define ZA_OFFSET_L       0x7E

// Using the MPU9250Teensy 3.1 Add-On shield, ADO is set to 0
// Seven-bit device address is 110100 for ADO = 0 and 110101 for ADO = 1
#define ADO 0
#if ADO
#define MPU9250_ADDRESS   0x69  // Device address when ADO = 1
#define AK8963_ADDRESS    0x0C  // Address of magnetometer
#define MS5637_ADDRESS    0x76  // Address of altimeter
#else
#define MPU9250_ADDRESS   0x68  // Device address when ADO = 0
#define AK8963_ADDRESS    0x0C  // Address of magnetometer
#define MS5637_ADDRESS    0x76  // Address of altimeter
#endif

#define SerialDebug       false  // set to true to get Serial output for debugging
#define UseDisplay        false

#define QBUFFER_SIZE        10

#include <Arduino.h>
#include <WString.h>
#include <i2c_t3.h>
#include "air_01.hpp"
#include "AirOSC.hpp"
#include "AirI2c.hpp"
#include "Oled.hpp"
#include "util.h"


// Set initial input parameters
enum Ascale
{
    AFS_2G = 0,
    AFS_4G,
    AFS_8G,
    AFS_16G
};

enum Gscale
{
    GFS_250DPS = 0,
    GFS_500DPS,
    GFS_1000DPS,
    GFS_2000DPS
};

enum Mscale
{
    MFS_14BITS = 0, // 0.6 mG per LSB
    MFS_16BITS      // 0.15 mG per LSB
};

#define ADC_256  0x00 // define pressure and temperature conversion rates
#define ADC_512  0x02
#define ADC_1024 0x04
#define ADC_2048 0x06
#define ADC_4096 0x08
#define ADC_8192 0x0A
#define ADC_D1   0x40
#define ADC_D2   0x50

class AirImu
{
public:
    enum ErrorCodes
    {
        SUCCESS,
        SELF_TEST_FAILED_ERROR,
        NO_COMMUNICATION_ERROR,
        UNKNOWN_ERROR,
    };
    void hook();
    static void setup();
    static void imu0_jt();
    static void imu1_jt();
    void interruptHandler();
    static AirImu *Sensor0;
    static AirImu *Sensor1;
    static uint32_t m_viewLastUpdate;       // used to control display output rate
    static void viewHook();
    void sample();
    volatile bool m_newData = false;        // on interrupt this flag is set to true

    void sync();

    float m_pitch, m_yaw, m_roll = 0.f;
    float m_ax, m_ay, m_az,
        m_gx, m_gy, m_gz,
        m_mx, m_my, m_mz = 0.f; // variables to hold latest sensor data values
    float m_lin_ax_norm, m_lin_ay_norm, m_lin_az_norm,
        m_gx_norm, m_gy_norm, m_gz_norm,
        m_mx_norm, m_my_norm, m_mz_norm,
        m_acc_norm,
        m_gyr_norm,
        m_mag_norm,
        m_yaw_norm,
        m_pitch_norm,
        m_roll_norm = 0.f; // variables to hold latest sensor data values
    float m_up[3], m_forward[3], m_right[3] = {0, 0, 0};

    float m_lin_ax, m_lin_ay, m_lin_az = 0.f; // linear acceleration (acceleration with gravity component subtracted)
    float m_q[4] = {1.0f, 0.0f, 0.0f, 0.0f}; // vector to hold quaternion
    // rotation matrix coefficients for Euler angles and gravity components
    float m_a12, m_a22, m_a31, m_a32, m_a33 = 0.f;
    float m_filterRateMax;                            // calculated sample rate
    float m_filterRateActual;                            // calculated sample rate
    float m_sampleRateMax;
    float m_sampleRateActual;
    Quaternion m_qAvg;

protected:
    AirImu(byte addr, uint8_t intPin, i2c_t3 *bus, String label);
    uint8_t m_addr;                    // the i2c address of the sensor
    String m_label;                    // brief label to identify the sensor
    i2c_t3 *m_bus;                     // the bus the sensor is on
    uint8_t m_osr = ADC_8192;          // set pressure amd temperature oversample rate
    uint8_t m_gScale = GFS_250DPS;
    uint8_t m_aScale = AFS_2G;
    uint8_t m_mScale = MFS_16BITS;     // Choose either 14-bit or 16-bit magnetometer resolution
    uint8_t m_mMode = 0x06;            // 2 for 8 Hz, 6 for 100 Hz continuous magnetometer data read
    float m_aRes, m_gRes, m_mRes;      // scale resolutions per LSB for the sensors
    uint8_t m_intPin;                  // the interrupt pin the sensor is connected to
    bool m_newMagData = false;         // on interrupt this flag is set to true
    uint16_t m_pCal[8];                // calibration constants from MS5637 PROM registers
    uint8_t m_nCrc;                    // calculated check sum to ensure PROM integrity
    uint32_t m_d1, m_d2 = 0;           // raw MS5637 pressure and temperature data
    float m_dt, m_offset, m_sens, m_t2, m_offset2, m_sens2;
    int16_t m_mpu9250Data[7];          // used to read all 14 bytes at once from the MPU9250 accel/gyro
    int16_t m_accelCount[3];           // Stores the 16-bit signed accelerometer sensor output
    int16_t m_gyroCount[3];            // Stores the 16-bit signed gyro sensor output
    int16_t m_magCount[3];             // Stores the 16-bit signed magnetometer sensor output
    float m_magCalibration[3] = {0, 0, 0};  // Factory mag calibration and mag bias
    float m_magBias[3] = {0, 0, 0};
    float m_magScale[3] = {0, 0, 0};
    float m_gyroBias[3] = {0, 0, 0};   // Bias corrections for gyro and accelerometer
    float m_accelBias[3] = {0, 0, 0};
    int16_t m_tempCount;               // temperature raw m_sampleElapsed output
    float m_chip_temperature;          // Stores the MPU9250 gyro internal chip temperature in degrees Celsius
    float m_temperature;               // stores MS5637 sensor temperature
    float m_pressure;                  // stores MS5637 sensor pressure
    float m_altitude;
    float m_selfTest[6];               // holds results of gyro and accelerometer self test
    float m_gyroMeasError;
    float m_gyroMeasDrift;
    float m_beta;
    float m_zeta;
    float m_kp;
    float m_ki;
    uint32_t m_now = 0;                    // used to calculate integration interval

    uint32_t m_filterElapsed = 0;          // time spent inside sample()
    float m_filterDeltaTime = 0;
    float m_filterDeltaSum = 0;        // integration interval for both filter schemes
    uint32_t m_filterLastUpdate = 0;       // used to calculate integration interval
    uint32_t m_filterCount = 0;            // used to control display output rate

    uint32_t m_sampleElapsed = 0;          // time spent inside sample()
    float m_sampleDeltaTime = 0;
    float m_sampleDeltaSum = 0;        // integration interval for both filter schemes
    uint32_t m_sampleLastUpdate = 0;       // time sample() was called last
    uint32_t m_sampleCount = 0;            // sample() iteration counter

    uint32_t m_intFirstUpdate = 0;        // used to calculate integration interval

    float m_eInt[3] = {0.0f, 0.0f, 0.0f};   // vector to hold integral error for Mahony method


    String m_tmp = "";

    int init(void (*isr)());
    void getMres();
    void getGres();
    void getAres();
    void MPU9250SelfTest(float *destination);
    void initMPU9250();
    void magcalMPU9250(float *dest1, float *dest2);
    void accelgyrocalMPU9250(float *dest1, float *dest2);
    void readMPU9250Data(int16_t *destination);
    void readAccelData(int16_t *destination);
    void readGyroData(int16_t *destination);
    void readMagData(int16_t *destination);
    int16_t readTempData();
    void initAK8963(float *destination);
    void readBytes(i2c_t3 *bus, uint8_t address, uint8_t subAddress, uint8_t count, uint8_t *dest);
    void writeByte(i2c_t3 *bus, uint8_t address, uint8_t subAddress, uint8_t data);
    void MS5637PromRead(i2c_t3 *bus, uint16_t *destination);
    void MS5637Reset(i2c_t3 *bus);
    uint8_t readByte(i2c_t3 *bus, uint8_t address, uint8_t subAddress);
    uint8_t MS5637checkCRC(uint16_t *n_prom);
    uint32_t MS5637Read(i2c_t3 *bus, uint8_t CMD, uint8_t OSR);
    void MahonyQuaternionUpdate
        (float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);
    void MadgwickQuaternionUpdate
        (float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);
    uint32_t m_syncLastUpdate;
    const uint32_t SYNC_RATE = SENSOR_UPDATE_RATE; // 5000 µs --> 200 Hz
    Quaternion qBuffer[QBUFFER_SIZE];
    uint32_t qBufferCursor;
    Quaternion getAvg(const Quaternion pQuaternion[QBUFFER_SIZE], uint32_t cursor, const int count);

    uint32_t m_filterLastReport;
};

#endif