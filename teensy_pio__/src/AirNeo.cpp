//
// Created by Gerold Schneider on 16.12.17.
//

#include "AirNeo.h"

void AirNeo::setup()
{
    AirNeo::Pixel = new Adafruit_NeoPixel(AIR_NEO_NUM_PIXELS, AIR_NEO_PIN, NEO_RGBW + NEO_KHZ400);
    AirNeo::Pixel->begin();
    AirNeo::Pixel->setPixelColor(0, 0, 150, 0, 0);
    AirNeo::Pixel->setPixelColor(1, 0, 150, 0, 0);
    AirNeo::Pixel->show();
}

void AirNeo::hook()
{
    AirNeo::Pixel->setPixelColor(0, 96, 150, 54, 0);
    AirNeo::Pixel->setPixelColor(1, 96, 150, 54, 0);
    AirNeo::Pixel->show();
}

AirNeo::AirNeo()
{
    init();
}

void AirNeo::init()
{

}
