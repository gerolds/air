//
// Created by Gerold Schneider on 07.12.17.
//

#ifndef TEENSY_PIO_D_TASTER_H
#define TEENSY_PIO_D_TASTER_H

#define UX_0_PIN 20
#define UX_1_PIN 21
#define PLAY_0_PIN 22
#define PLAY_1_PIN 23

#include "Arduino.h"
#include "Oled.hpp"
#include "air_01.hpp"

class AirSwitch
{
    uint8_t m_ux0Pin;
    uint8_t m_ux1Pin;
    uint8_t m_play0Pin;
    uint8_t m_play1Pin;
public:
    AirSwitch(uint8_t ux_0_pin, uint8_t ux_1_pin, uint8_t play_0_pin, uint8_t play_1_pin);
    static void setup();
    void init();
    void hook();
    void viewHook();
    void ux0IntHandler();
    void ux1IntHandler();
    void play0IntHandler();
    void play1IntHandler();
    static AirSwitch *Singleton;
    uint8_t ux0;
    uint8_t ux1;
    uint8_t play0;
    uint8_t play1;
private:
    static void ux_0_jt();
    static void ux_1_jt();
    static void play_0_jt();
    static void play_1_jt();
    uint32_t m_lastUpdate;
};

extern AirSwitch *Taster;

// static jump tables for interrupts


#endif //TEENSY_PIO_D_TASTER_H
