//
// Created by Gerold Schneider on 06.12.17.
//

#ifndef TEENSY_PIO_D_SPI_H
#define TEENSY_PIO_D_SPI_H

#include <Arduino.h>
#include <SPI.h>

void spi_setup();

#endif //TEENSY_PIO_D_SPI_H
