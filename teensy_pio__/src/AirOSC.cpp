//
// Created by Gerold Schneider on 07.12.17.
//

#include "AirOSC.hpp"
#include "AirSwitch.h"

SLIPEncodedUSBSerial *(AirOSC::Slip);

AirOSC *(AirOSC::Singleton);

void AirOSC::setup()
{
    Singleton = new AirOSC();
    Slip = new SLIPEncodedUSBSerial(thisBoardsSerialUSB);
    Slip->begin(SERIAL_BAUD);
}

void AirOSC::init()
{
    m_oscDeltaTime = 0;
    m_oscLastMessage = 0;
    m_slipByteSum = 0;
    m_msgCount = 0;
}

// returns the size of a given bundle in bytes
uint32_t getBytes(OSCBundle *bundle)
{
    uint32_t bundleSize = 0;
    int numMessages = bundle->size();
    for (int i = 0; i < numMessages; i++) {
        bundleSize += bundle->getOSCMessage(i)->bytes();
    }
    return bundleSize;
}

void AirOSC::hook()
{
    /****************************************************************************
    OSC Messaging
    ****************************************************************************/

    m_oscDeltaTime = micros() - m_oscLastMessage;
    if (m_oscDeltaTime > MESSAGE_RATE) {
        m_oscLastMessage = micros();

        if (OSCSend) {
            OSCBundle bundle;
            bundle.add("/sq").add(m_msgCount);
            bundle.add("/ts").add(oscTime());
            bundle.add("/ms").add((unsigned int)millis());
            bundle.setTimetag(oscTime());
            bundle.add("/right/imu/0/qt")
                .add(AirImu::Sensor0->m_q[0])
                .add(AirImu::Sensor0->m_q[1])
                .add(AirImu::Sensor0->m_q[2])
                .add(AirImu::Sensor0->m_q[3]);
            bundle.add("/right/imu/0/ac")
                .add(AirImu::Sensor0->m_lin_ax_norm)
                .add(AirImu::Sensor0->m_lin_ay_norm)
                .add(AirImu::Sensor0->m_lin_az_norm);
            bundle.add("/right/imu/0/gy")
                .add(AirImu::Sensor0->m_gx_norm)
                .add(AirImu::Sensor0->m_gy_norm)
                .add(AirImu::Sensor0->m_gz_norm);
            bundle.add("/right/imu/0/ypr")
                .add(AirImu::Sensor0->m_yaw_norm)
                .add(AirImu::Sensor0->m_pitch_norm)
                .add(AirImu::Sensor0->m_roll_norm);
//            bundle.add("/right/imu/0/grav")
//                .add(-AirImu::Sensor0->m_a31)
//                .add(-AirImu::Sensor0->m_a32)
//                .add(AirImu::Sensor0->m_a33);
//            bundle.add("/right/imu/0/gyNorm").add(AirImu::Sensor0->m_gyr_norm);
//            bundle.add("/right/imu/0/acNorm").add(AirImu::Sensor0->m_acc_norm);
//            bundle.add("/right/imu/0/maNorm").add(AirImu::Sensor0->m_mag_norm);
//            bundle.add("/right/imu/0/filterRate").add(AirImu::Sensor0->m_filterRateActual);
            bundle.add("/right/imu/1/qt")
                .add(AirImu::Sensor1->m_q[0])
                .add(AirImu::Sensor1->m_q[1])
                .add(AirImu::Sensor1->m_q[2])
                .add(AirImu::Sensor1->m_q[3]);
            bundle.add("/right/imu/1/ac")
                .add(AirImu::Sensor1->m_lin_ax_norm)
                .add(AirImu::Sensor1->m_lin_ay_norm)
                .add(AirImu::Sensor1->m_lin_az_norm);
            bundle.add("/right/imu/1/gy")
                .add(AirImu::Sensor1->m_gx_norm)
                .add(AirImu::Sensor1->m_gy_norm)
                .add(AirImu::Sensor1->m_gz_norm);
            bundle.add("/right/imu/1/ypr")
                .add(AirImu::Sensor1->m_yaw_norm)
                .add(AirImu::Sensor1->m_pitch_norm)
                .add(AirImu::Sensor1->m_roll_norm);
//            bundle.add("/right/imu/1/grav")
//                .add(-AirImu::Sensor1->m_a31)
//                .add(-AirImu::Sensor1->m_a32)
//                .add(AirImu::Sensor1->m_a33);
//            bundle.add("/right/imu/1/gyNorm").add(AirImu::Sensor1->m_gyr_norm);
//            bundle.add("/right/imu/1/acNorm").add(AirImu::Sensor1->m_acc_norm);
//            bundle.add("/right/imu/1/maNorm").add(AirImu::Sensor1->m_mag_norm);
//            bundle.add("/right/imu/1/filterRate").add(AirImu::Sensor1->m_filterRateActual);
            bundle.add("/right/sw")
                .add((float) AirSwitch::Singleton->ux0)
                .add((float) AirSwitch::Singleton->ux1)
                .add((float) AirSwitch::Singleton->play0)
                .add((float) AirSwitch::Singleton->play1);

            AirOSC::Slip->beginPacket();
            bundle.send(*AirOSC::Slip); // send the bytes to the SLIP stream
            AirOSC::Slip->endPacket(); // mark the end of the OSC Packet
            bundle.empty(); // empty the bundle to free room for a new one

            m_slipByteSum += getBytes(&bundle);
            m_slipSumCount++;
            m_slipElapsed += micros() - m_oscLastMessage;
        }
    }

    //            Vector3 ac0 = {
//                AirImu::Sensor0->m_lin_ax_norm,
//                AirImu::Sensor0->m_lin_ay_norm,
//                AirImu::Sensor0->m_lin_az_norm
//            };
//
//            Vector3 ac1 = {
//                AirImu::Sensor1->m_lin_ax_norm,
//                AirImu::Sensor1->m_lin_ay_norm,
//                AirImu::Sensor1->m_lin_az_norm
//            };
//
//            Vector3 acCo = {
//                ac0.x - ac1.x,
//                ac0.y - ac1.y,
//                ac0.z - ac1.z
//            };
//
//            Vector3 gy0 = {
//                AirImu::Sensor0->m_gx_norm,
//                AirImu::Sensor0->m_gy_norm,
//                AirImu::Sensor0->m_gz_norm
//            };
//
//            Vector3 gy1 = {
//                AirImu::Sensor1->m_gx_norm,
//                AirImu::Sensor1->m_gy_norm,
//                AirImu::Sensor1->m_gz_norm
//            };
//
//            Vector3 gyCo = {
//                gy0.x - gy1.x,
//                gy0.y - gy1.y,
//                gy0.z - gy1.z
//            };
//
//            Quaternion q0 = {
//                AirImu::Sensor0->m_q[0],
//                AirImu::Sensor0->m_q[1],
//                AirImu::Sensor0->m_q[2],
//                AirImu::Sensor0->m_q[3]
//            };
//
//            Vector3 fwd = {0,0,1};
//            Vector3 up = {0,1,0};
//            Vector3 right = {1,0,0};

//            bundle.add("/right/imu/co/ac").add(acCo.x).add(acCo.y).add(acCo.z);
//            bundle.add("/right/imu/co/acDist").add(Util::distance(ac0, ac1));
//            bundle.add("/right/imu/co/acNorm").add(Util::norm(acCo));
//            bundle.add("/right/imu/co/acDot").add(Util::dot(ac0, ac1));
//            Vector3 acCoCross = Util::cross(ac0, ac1);
//            bundle.add("/right/imu/co/acCross").add(acCoCross.x).add(acCoCross.y).add(acCoCross.z);
//
//            bundle.add("/right/imu/co/gy").add(gyCo.x).add(gyCo.y).add(gyCo.z);
//            bundle.add("/right/imu/co/gyDist").add(Util::distance(gy0, gy1));
//            bundle.add("/right/imu/co/gyNorm").add(Util::norm(gyCo));
//            bundle.add("/right/imu/co/gyDot").add(Util::dot(gy0, gy1));
//            Vector3 gyCoCross = Util::cross(gy0, gy1);
//            bundle.add("/right/imu/co/gyCross").add(gyCoCross.x).add(gyCoCross.y).add(gyCoCross.z);
//            bundle.add("/right/imu/co/gyDot").add(Util::dot(gy0, gy1));

    m_displayDeltaTime = micros() - m_displayLastUpdate;
    m_msgCount++;
    if (m_displayDeltaTime > DIAG_UPDATE_RATE) {
        m_displayLastUpdate = micros();

        m_byteRate = (float) m_slipByteSum / KILO_F / ((float) m_displayDeltaTime / MEGA_F);
        // ((display_interval / 1000000.f) / 1000.f); // KByte
        m_byteMsgAvg = m_slipByteSum / m_slipSumCount; // Byte
        m_msgRate = (float) m_slipSumCount / ((float) m_displayDeltaTime / MEGA_F); // Hz
        m_msgMaxRate = (float) m_slipSumCount / ((float) m_slipElapsed / (float) m_displayDeltaTime); // Hz

        Oled::Singleton->setCursor(0, 6 * 8);
        Oled::Singleton->print("                ");
        Oled::Singleton->setCursor(0, 6 * 8);
        Oled::Singleton->print(m_byteRate, 0);
        Oled::Singleton->print("KBs ");
        Oled::Singleton->print(m_msgRate, 0);
        Oled::Singleton->print("(");
        Oled::Singleton->print(m_msgMaxRate, 0);
        Oled::Singleton->print(")Hz");

        if (m_msgMaxRate < (m_msgRate * 2))
            Oled::Singleton->print(" !!!");

        m_slipByteSum = 0;
        m_slipSumCount = 0;
        m_slipElapsed = 0;
    }
}

AirOSC::AirOSC()
{
    init();
}
