//
// Created by Gerold Schneider on 06.12.17.
//

#include "air_01.hpp"

const unsigned int led = 13;

long main_led_blink = 0;

bool main_led_toggle = false;

uint_fast32_t glob_loopDeltaSum;

uint_fast32_t glob_loopCount;

uint_fast32_t glob_lastLoop;

uint_fast32_t glob_lastDisplayUpdate;

uint_fast32_t glob_loopDetaTime;

Adafruit_NeoPixel neo(1, 6, NEO_GRBW + NEO_KHZ400);

uint_fast32_t glob_loopElapsed;

uint_fast32_t glob_diagLastUpdate;

void setup()
{

    digitalWrite(13, HIGH);

    Serial.begin(SERIAL_BAUD);

    Serial.println("Starting up ...");

    spi_setup();

    // spi kills the led, need to reset it
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);

    Oled::setup();
    Oled::Singleton->push("SPI   OK");
    Serial.println("SPI OK");
    Oled::Singleton->push("OLED  OK");
    Serial.println("OLED OK");

    AirOSC::setup();
    Oled::Singleton->push("SLIP  OK");
    Oled::Singleton->push("OSC   OK");
    Serial.println("SLIP OK");
    Serial.println("OSC  OK");

    AirEncoder::setup(&encIsrRotation);
    Oled::Singleton->push("ENC   OK");
    Serial.println("ENC OK");

    AirSwitch::setup();
    AirSwitch::Singleton->init();
    Oled::Singleton->push("TAST  OK");
    Serial.println("TAST OK");

    neo.begin();
    neo.show();
    neo.setBrightness(64);
    neo.setPixelColor(0, neo.Color(0, 150, 0, 150));
    neo.show();
    Oled::Singleton->push("NEO   OK");
    Serial.println("NEO OK");


    AirI2c::setup();
    AirI2c::scan(&Wire);
    AirI2c::scan(&Wire1);
    Oled::Singleton->push("I2C   OK");
    Serial.println("I2C OK");
    delay(500);
    //delay(5000);

    AirImu::setup();
    Oled::Singleton->push("IMU   OK");
    Serial.println("IMU OK");

    //Lidar::setup();
    //Oled::Singleton->push("LIDAR OK");

    Oled::Singleton->push("air 0.1");
    Oled::Singleton->push("Ready");
    Serial.println("air 0.1");
    Serial.println("Ready");
    delay(500);

    Oled::Singleton->clearDisplay();
    Oled::Singleton->resetStart();
}

void loop()
{
    //sol_update_toggle();
    glob_loopDetaTime = micros() - glob_lastLoop;
    glob_lastLoop = micros();
    glob_loopCount++;

    AirImu::Sensor0->sync();
    AirImu::Sensor1->sync();
    AirImu::Sensor0->hook();
    AirImu::Sensor1->hook();
    AirImu::viewHook();
    AirEncoder::Singleton->hook();
    AirSwitch::Singleton->hook();
    //AirNeo::hook();

    AirOSC::Singleton->hook();

    if (micros() - glob_lastDisplayUpdate > DISPLAY_UPDATE_RATE) {
        //AirImu::Sensor0->m_newData = true;
        glob_lastDisplayUpdate = micros();
        Oled::Singleton->display();

        //Serial.print((float) glob_loopCount / (float) glob_loopDeltaSum * 1000.f, 3);
        //Serial.println("kHz");

        if (main_led_toggle) {
            digitalWrite(led, HIGH);
            main_led_toggle = false;
        }
        else {
            digitalWrite(led, LOW);
            main_led_toggle = true;
        }

    }

    if (micros() - glob_diagLastUpdate > DIAG_UPDATE_RATE) {
        uint32_t timeSinceLastUpdate = micros() - glob_diagLastUpdate;
        uint32_t avgTimePerLoop = timeSinceLastUpdate / glob_loopCount;
        uint32_t loopsPerSecond = MEGA / avgTimePerLoop;
        Oled::Singleton->setCursor(0, 7 * 8);
        Oled::Singleton->print(loopsPerSecond);
        Oled::Singleton->print("Hz ");
        Oled::Singleton->print(avgTimePerLoop / 1000);
        Oled::Singleton->print("ms ");
        Oled::Singleton->print((float) millis() / 1000., 1);
        Oled::Singleton->println("s");

        glob_diagLastUpdate = micros();
        glob_loopCount = 0;

    }

}