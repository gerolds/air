#include "AirEncoder.h"
#include <Adafruit_SSD1306.h>
#include "AirEncoder.h"

// RotaryEncoder encoder(ENCODER_A_PIN, ENCODER_B_PIN, 5);

AirEncoder *(AirEncoder::Singleton);

void AirEncoder::hook()
{

//    if (micros() - m_lastPollTime > ENCODER_POLL_RATE) {
//        m_lastPollTime = micros();
//        isr();
//    }

    if (millis() - m_lastDispTime > ENCODER_DISP_RATE) {
        m_lastDispTime = millis();
        int32_t movement = encoder_getChange();
        //bool sw = (digitalRead(ENCODER_SW_PIN) == HIGH);
        //bool a = (digitalRead(ENCODER_A_PIN) == HIGH);
        //bool b = (digitalRead(ENCODER_B_PIN) == HIGH);
        m_count += movement;
        Oled::Singleton->setCursor(0, 5 * 8);
        Oled::Singleton->println("                    ");
        Oled::Singleton->setCursor(0, 5 * 8);
        Oled::Singleton->print("#");
        Oled::Singleton->print(m_count);
        Oled::Singleton->print(" a");
        Oled::Singleton->print(m_a);
        Oled::Singleton->print(" b");
        Oled::Singleton->print(m_b);
        Oled::Singleton->print(" sw");
        Oled::Singleton->print(m_sw);
    }
}

int AirEncoder::encoder_getChange()
{
    int r;
    noInterrupts();
    if (m_change >= m_ppc - 1) {
        r = (m_change + 1) / m_ppc;
    }
    else if (m_change <= 1 - m_ppc) {
        r = -((1 - m_change) / m_ppc);
    }
    else {
        r = 0;
    }
    m_change -= (r * m_ppc);
    interrupts();
    return r;
}

void AirEncoder::pollSw()
{
    m_sw = digitalRead(AirEncoder::Singleton->m_swPin);
}

void AirEncoder::poll()
{
    // State transition table

    uint8_t t = encoder_readState();
    int mov = 0;
    if (t != m_state) {
        if (m_state == 0) {
            if (t == 1) mov = 1;
            if (t == 2) mov = -1;
            if (t == 3) mov = 1;
        }
        if (m_state == 1) {
            if (t == 0) mov = -1;
            if (t == 2) mov = 1;
            if (t == 3) mov = 1;
        }
        if (m_state == 2) {
            if (t == 0) mov = 1;
            if (t == 1) mov = -1;
            if (t == 3) mov = -1;
        }
        if (m_state == 3) {
            if (t == 0) mov = -1;
            if (t == 1) mov = 1;
            if (t == 2) mov = -1;
        }
        m_change += mov;
        m_state = t;
        m_sw = digitalRead(m_swPin);
    }
}

uint8_t AirEncoder::encoder_readState()
{
    m_a = digitalRead(ENCODER_A_PIN);
    m_b = digitalRead(ENCODER_B_PIN);
    return (uint8_t) ((m_a > 0 ? 1u : 0u) | (m_b > 0 ? 2u : 0u));
}

void AirEncoder::setup(void (*isr)())
{
    AirEncoder::Singleton = new AirEncoder(ENCODER_A_PIN,
                                           ENCODER_B_PIN,
                                           ENCODER_SW_PIN,
                                           ENCODER_RED_PIN,
                                           ENCODER_GREEN_PIN,
                                           ENCODER_BLUE_PIN,
                                           4);
}

void AirEncoder::init()
{
    if (rgb)
        AirEncoder::Singleton->setColor(255, 195, 127);
    return;
}

AirEncoder::AirEncoder(uint8_t aPin, uint8_t bPin, uint8_t swPin, uint8_t ppc)
    : AirEncoder(aPin, bPin, swPin, ppc, 0, 0, 0)
{
    rgb = false;
}

AirEncoder::AirEncoder(uint8_t aPin,
                       uint8_t bPin,
                       uint8_t swPin,
                       uint8_t redPin,
                       uint8_t greenPin,
                       uint8_t bluePin,
                       uint8_t ppc)
    : m_aPin(aPin), m_bPin(bPin), m_swPin(swPin), m_redPin(redPin), m_greenPin(greenPin), m_bluePin(bluePin), m_ppc(ppc)
{
    if (rgb) {
        pinMode(redPin, OUTPUT);
        pinMode(greenPin, OUTPUT);
        pinMode(bluePin, OUTPUT);
    }
    pinMode(aPin, INPUT);
    pinMode(bPin, INPUT);
    pinMode(swPin, INPUT);
    attachInterrupt(aPin, &encIsrRotation, CHANGE);
    attachInterrupt(bPin, &encIsrRotation, CHANGE);
    attachInterrupt(swPin, &encIsrSwitch, CHANGE);
    init();
}

void AirEncoder::setColor(uint8_t r, uint8_t g, uint8_t b)
{
    if (rgb) {
        analogWrite(ENCODER_RED_PIN, r);
        analogWrite(ENCODER_GREEN_PIN, g);
        analogWrite(ENCODER_BLUE_PIN, b);
    }
}

// interrupts need a static function to work. One for each encoder.
void encIsrRotation()
{
    AirEncoder::Singleton->poll();
}

// interrupts need a static function to work. One for each encoder.
void encIsrSwitch()
{
    AirEncoder::Singleton->pollSw();
}
