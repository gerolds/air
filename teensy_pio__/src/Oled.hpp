//
// Created by Gerold Schneider on 06.12.17.
//

#ifndef TEENSY_PIO_D_OLED_H
#define TEENSY_PIO_D_OLED_H

#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include "constants.hpp"

// OLED SPI PINS
#define OLED_DC     9
#define OLED_CS     10
#define OLED_RESET  15

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

class Oled: public Adafruit_SSD1306
{

public:
    static Oled *Singleton;
    void resetStart();
    void hook();
    void drawBMP(const uint8_t *bitmap, uint8_t x, uint8_t y, uint8_t w, uint8_t h);
    static void setup();
    void push(String msg);
//    void add(String *msg);
//    void push(String *msg, bool scroll);
//    void push(bool scroll);
//    void flush();
    uint8_t bufferCursor = 0;
    String buffer = "                    ";
private:
    Oled(int8_t SID, int8_t SCLK, int8_t DC, int8_t RST, int8_t CS);
    Oled(int8_t DC, int8_t RST, int8_t CS);
    Oled(int8_t RST = -1);
    Oled();
    void onHook();
    void onResetStart();
    void onPush(String msg);
    void onDrawBMP(const uint8_t *bitmap, uint8_t x, uint8_t y, uint8_t w, uint8_t h);
    void init();

    uint16_t spos = 0;

};

#endif //TEENSY_PIO_D_OLED_H
