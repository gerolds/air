//
// Created by Gerold Schneider on 07.12.17.
//

#ifndef TEENSY_PIO_D_ENCODER_H
#define TEENSY_PIO_D_ENCODER_H

#include <Arduino.h>
#include "Oled.hpp"

#define ENCODER_RED_PIN   3
#define ENCODER_GREEN_PIN 4
#define ENCODER_BLUE_PIN  5
#define ENCODER_A_PIN     7
#define ENCODER_B_PIN     8
#define ENCODER_SW_PIN    2
#define ENCODER_POLL_RATE 100 // µs
#define ENCODER_DISP_RATE 100 // ms

class AirEncoder
{
public:
    AirEncoder
        (uint8_t aPin, uint8_t bPin, uint8_t swPin, uint8_t redPin, uint8_t greenPin, uint8_t bluePin, uint8_t ppc);
    AirEncoder(uint8_t aPin, uint8_t bPin, uint8_t swPin, uint8_t ppc);
    //static void setup();
    static void setup(void (*isr)());
    void hook();
    void setColor(uint8_t r, uint8_t g, uint8_t b);
    void poll();
    static AirEncoder *Singleton;
    void pollSw();
private:
    bool rgb = true;
    uint8_t m_aPin;
    uint8_t m_bPin;
    uint8_t m_swPin;
    uint8_t m_redPin;
    uint8_t m_greenPin;
    uint8_t m_bluePin;
    uint8_t m_ppc;
    uint32_t m_count = 0;
    uint32_t m_lastPollTime = 0;
    uint32_t m_lastDispTime = 0;
    int32_t m_change = 0;
    uint8_t m_state = 0;
    uint8_t m_sw = 0;
    uint8_t m_a = 0;
    uint8_t m_b = 0;

    void init();
    int encoder_getChange();
    uint8_t encoder_readState();

};

extern AirEncoder *Encoder;
extern void encIsrRotation();
extern void encIsrSwitch();


#endif //TEENSY_PIO_D_ENCODER_H
