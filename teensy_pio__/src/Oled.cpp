#include "../../../../../../../.platformio/packages/framework-arduinoteensy/cores/teensy3/WString.h"

#include "Oled.hpp"

Oled::Oled(int8_t RST)
    : Adafruit_SSD1306(RST)
{
}

Oled::Oled(int8_t DC, int8_t RST, int8_t CS)
    : Adafruit_SSD1306(DC, RST, CS)
{
}

Oled::Oled(int8_t SID, int8_t SCLK, int8_t DC, int8_t RST, int8_t CS)
    : Adafruit_SSD1306(SID, SCLK, DC, RST, CS)
{
}

Oled::Oled()
    : Adafruit_SSD1306(OLED_DC, OLED_RESET, OLED_CS)
{

}

// Integrate the device into the local environment (called from the main setup()-routine)
void Oled::setup()
{
    Singleton = new Oled(OLED_DC, OLED_RESET, OLED_CS);

    Singleton->init();
}

Oled *(Oled::Singleton);

void Oled::push(String msg)
{
    setTextSize(1);
    setCursor(0, spos % 64);
    setTextColor(WHITE, BLACK);
    print("                     ");
    //display.print("012345678901234567890");
    setCursor(0, spos % 64);
// scroll old screen buffer out of the way (1 line)
//  for (int i = 0; i < 8; ++i) {
//    //delay(5);
//    display.ssd1306_command(SSD1306_SETSTARTLINE | spos % 64);
//    spos++;
//  }
    spos += 8;
    ssd1306_command(SSD1306_SETSTARTLINE | spos % 64);
    // print the new line
    print(msg);
    display();
}

void Oled::resetStart()
{
    onResetStart();
}

void Oled::onResetStart()
{
    spos = 0;
    ssd1306_command(SSD1306_SETSTARTLINE);
}

void Oled::hook()
{
    Singleton->onHook();
}

void Oled::onHook()
{
    // Loop code
}

void Oled::drawBMP(const uint8_t *bitmap, uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
    onDrawBMP(bitmap, x, y, w, h);
}

void Oled::onDrawBMP(const uint8_t *bitmap, uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
    drawBitmap(x, y, bitmap, w, h, WHITE);
    display();
}

// Make the device ready for use
void Oled::init()
{
    // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
    // display.begin(SSD1306_SWITCHCAPVCC, 0x3D);  // initialize with the I2C addr 0x3D (for the 128x64)
    begin(SSD1306_SWITCHCAPVCC, 0x3D); // setup for SPI

    // setup done

    // Show image buffer on the display hardware.
    // Since the buffer is intialized with an Adafruit splashscreen
    // internally, this will display the splashscreen.
    clearDisplay();
    drawBMP(logo64_air, 0, 0, 128, 64);
    display();
    delay(1000);
    stopscroll();

    // invert the display
    invertDisplay(true);
    delay(250);
    invertDisplay(false);
    delay(250);
    clearDisplay();

    // text display tests
    setTextSize(1);
    setTextColor(WHITE, BLACK);

    //oled_push("")
    // display.setTextColor(BLACK, WHITE); // 'inverted' text
    // display.setTextSize(2);
}

//void Oled::push(bool scroll = false)
//{
//    setTextSize(1);
//    setCursor(0, spos % 64);
//    setTextColor(WHITE, BLACK);
//    setCursor(0, spos % 64);
//    // scroll old screen buffer out of the way (1 line)
//    if (scroll) {
//        for (int i = 0; i < 8; ++i) {
//            delay(5);
//            ssd1306_command(SSD1306_SETSTARTLINE | spos % 64);
//            spos++;
//        }
//    }
//    else {
//        spos += 8;
//        ssd1306_command(SSD1306_SETSTARTLINE | spos % 64);
//    }
//
//    // print the new line
//    print(buffer);
//    display();
//}

//void Oled::push(String *msg, bool scroll = true)
//{
//    setTextSize(1);
//    setCursor(0, spos % 64);
//    setTextColor(WHITE, BLACK);
//    setCursor(0, spos % 64);
//    // scroll old screen buffer out of the way (1 line)
//    if (scroll) {
//        for (int i = 0; i < 8; ++i) {
//            delay(5);
//            ssd1306_command(SSD1306_SETSTARTLINE | spos % 64);
//            spos++;
//        }
//    }
//    else {
//        spos += 8;
//        ssd1306_command(SSD1306_SETSTARTLINE | spos % 64);
//    }
//
//    // print the new line
//    flush();
//    add(msg);
//    print(buffer);
//    println("x");
//    display();
//    //flush();
//}
//
//void Oled::add(String *msg)
//{
//    uint_fast8_t count = min(20u - bufferCursor, msg->length());
//    Oled::buffer += msg->substring(0, count);
//    Oled::bufferCursor += count;
//
//    if (bufferCursor >= 20)
//        Oled::push();
//}
//
//void Oled::flush()
//{
//    buffer = "";
//}