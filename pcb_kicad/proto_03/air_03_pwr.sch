EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:air
LIBS:teensy
LIBS:SparkFun-IC-Power
LIBS:ESP8266
LIBS:SparkFun-Aesthetics
LIBS:SparkFun-Batteries
LIBS:SparkFun-Boards
LIBS:SparkFun-Capacitors
LIBS:SparkFun-Clocks
LIBS:SparkFun-Coils
LIBS:SparkFun-Connectors
LIBS:SparkFun-DiscreteSemi
LIBS:SparkFun-Displays
LIBS:SparkFun-Electromechanical
LIBS:SparkFun-Fuses
LIBS:SparkFun-GPS
LIBS:SparkFun-Hardware
LIBS:SparkFun-IC-Amplifiers
LIBS:SparkFun-IC-Comms
LIBS:SparkFun-IC-Conversion
LIBS:SparkFun-IC-Logic
LIBS:SparkFun-IC-Memory
LIBS:SparkFun-IC-Microcontroller
LIBS:SparkFun-IC-Special-Function
LIBS:SparkFun-Jumpers
LIBS:SparkFun-LED
LIBS:SparkFun-PowerSymbols
LIBS:SparkFun-Resistors
LIBS:SparkFun-RF
LIBS:SparkFun-Sensors
LIBS:SparkFun-Switches
LIBS:air_03B-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 2
Title "air 0.2 PCB"
Date "2017-12-26"
Rev "1"
Comp "Ylenia Gerold Emil"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L V_REG_AP2112K-3.3V U3
U 1 1 5A4D7433
P 8900 5800
F 0 "U3" H 8900 6125 45  0000 C TNN
F 1 "V_REG_AP2112K-3.3V" H 8900 5550 45  0000 C BNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 8900 5500 30  0000 C CNN
F 3 "" H 8900 6125 60  0001 C CNN
	1    8900 5800
	1    0    0    -1  
$EndComp
$Comp
L C_Small C3
U 1 1 5A4D7435
P 7150 5950
F 0 "C3" H 7160 6020 50  0000 L CNN
F 1 "10µF" H 7160 5870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 7150 5950 50  0001 C CNN
F 3 "" H 7150 5950 50  0001 C CNN
	1    7150 5950
	1    0    0    -1  
$EndComp
$Comp
L C_Small C4
U 1 1 5A4D7436
P 9450 5950
F 0 "C4" H 9460 6020 50  0000 L CNN
F 1 "10µF" H 9460 5870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 9450 5950 50  0001 C CNN
F 3 "" H 9450 5950 50  0001 C CNN
	1    9450 5950
	1    0    0    -1  
$EndComp
$Comp
L C_Small C6
U 1 1 5A4D7437
P 9700 5950
F 0 "C6" H 9710 6020 50  0000 L CNN
F 1 "1µF" H 9710 5870 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 9700 5950 50  0001 C CNN
F 3 "" H 9700 5950 50  0001 C CNN
	1    9700 5950
	1    0    0    -1  
$EndComp
Text Notes 9350 5350 2    60   ~ 0
POWER AND FILTERING
$Comp
L R_Small R12
U 1 1 5A4D748D
P 8000 5750
F 0 "R12" H 8030 5770 50  0000 L CNN
F 1 "100K" H 8030 5710 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 8000 5750 50  0001 C CNN
F 3 "" H 8000 5750 50  0001 C CNN
	1    8000 5750
	0    1    1    0   
$EndComp
$Comp
L GNDD #PWR60
U 1 1 5A4D748F
P 9700 6150
F 0 "#PWR60" H 9700 5900 50  0001 C CNN
F 1 "GNDD" H 9700 6025 50  0000 C CNN
F 2 "" H 9700 6150 50  0001 C CNN
F 3 "" H 9700 6150 50  0001 C CNN
	1    9700 6150
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR59
U 1 1 5A4D7490
P 9450 6150
F 0 "#PWR59" H 9450 5900 50  0001 C CNN
F 1 "GNDD" H 9450 6025 50  0000 C CNN
F 2 "" H 9450 6150 50  0001 C CNN
F 3 "" H 9450 6150 50  0001 C CNN
	1    9450 6150
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR58
U 1 1 5A4D7491
P 8450 6150
F 0 "#PWR58" H 8450 5900 50  0001 C CNN
F 1 "GNDD" H 8450 6025 50  0000 C CNN
F 2 "" H 8450 6150 50  0001 C CNN
F 3 "" H 8450 6150 50  0001 C CNN
	1    8450 6150
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR56
U 1 1 5A4D7492
P 7150 6150
F 0 "#PWR56" H 7150 5900 50  0001 C CNN
F 1 "GNDD" H 7150 6025 50  0000 C CNN
F 2 "" H 7150 6150 50  0001 C CNN
F 3 "" H 7150 6150 50  0001 C CNN
	1    7150 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 5550 7150 5850
Connection ~ 7150 5750
Wire Wire Line
	7150 6150 7150 6050
Wire Wire Line
	7150 5650 8550 5650
Connection ~ 7150 5650
Wire Wire Line
	9250 5650 10000 5650
Wire Wire Line
	9450 5650 9450 5850
Wire Wire Line
	9700 5650 9700 5850
Connection ~ 9450 5650
Wire Wire Line
	9700 6150 9700 6050
Wire Wire Line
	9450 6150 9450 6050
Connection ~ 9700 5650
Text HLabel 6600 5550 0    60   Input ~ 0
VBAT
Text HLabel 10000 5650 2    60   Output ~ 0
3V3
Text HLabel 6600 5750 0    60   Input ~ 0
VBUS
$Comp
L SWITCH-SPDT S4
U 1 1 5A4D8897
P 7650 5850
F 0 "S4" H 7650 6000 45  0000 C CNN
F 1 "SWITCH-SPDT" H 7650 5675 45  0000 C CNN
F 2 "air:SW_SPDT_PCM12SMTR" H 7650 5600 30  0001 C CNN
F 3 "" H 7550 6000 50  0001 C CNN
	1    7650 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5750 7900 5750
Wire Wire Line
	7450 5850 7550 5850
Wire Wire Line
	7450 5650 7450 5850
NoConn ~ 7850 5950
Wire Wire Line
	6600 5750 7150 5750
Connection ~ 7450 5650
$Comp
L R_Small R11
U 1 1 5A4D93CC
P 8250 6000
F 0 "R11" H 8280 6020 50  0000 L CNN
F 1 "100K" H 8280 5960 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 8250 6000 50  0001 C CNN
F 3 "" H 8250 6000 50  0001 C CNN
	1    8250 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	8550 5850 8450 5850
Wire Wire Line
	8450 5850 8450 6150
$Comp
L GNDD #PWR57
U 1 1 5A4D95E8
P 8250 6150
F 0 "#PWR57" H 8250 5900 50  0001 C CNN
F 1 "GNDD" H 8250 6025 50  0000 C CNN
F 2 "" H 8250 6150 50  0001 C CNN
F 3 "" H 8250 6150 50  0001 C CNN
	1    8250 6150
	1    0    0    -1  
$EndComp
Connection ~ 8250 5750
Wire Wire Line
	8250 5900 8250 5750
Wire Wire Line
	8250 6100 8250 6150
Wire Wire Line
	8100 5750 8550 5750
$Comp
L D_Schottky D5
U 1 1 5A4E7376
P 6850 5550
AR Path="/5A4D6CC1/5A4E7376" Ref="D5"  Part="1" 
AR Path="/5A4E7376" Ref="D5"  Part="1" 
AR Path="/5A4DAC7D/5A4E7376" Ref="D5"  Part="1" 
F 0 "D5" H 6850 5650 50  0000 C CNN
F 1 "MBR120" H 6850 5450 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123F" H 6850 5550 50  0001 C CNN
F 3 "" H 6850 5550 50  0001 C CNN
	1    6850 5550
	-1   0    0    1   
$EndComp
Wire Wire Line
	7150 5550 7000 5550
Wire Wire Line
	6700 5550 6600 5550
$EndSCHEMATC
