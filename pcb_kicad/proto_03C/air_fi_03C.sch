EESchema Schematic File Version 2
LIBS:air_fi_03-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:air
LIBS:air_fi_03-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MPU9250-RESCUE-air_fi_03 U1
U 1 1 5A50FFCC
P 4250 3750
F 0 "U1" H 4300 4250 60  0000 C CNN
F 1 "MPU9250" H 4250 3250 60  0000 C CNN
F 2 "Housings_DFN_QFN:QFN-24-1EP_3x3mm_Pitch0.4mm" H 4400 3250 60  0000 C CNN
F 3 "" H 4400 3250 60  0000 C CNN
	1    4250 3750
	1    0    0    -1  
$EndComp
$Comp
L Air_Conn_01x06 J1
U 1 1 5A510815
P 7150 4150
F 0 "J1" H 7150 4450 50  0000 C CNN
F 1 "Air_Conn_01x06" H 7300 3350 50  0000 C CNN
F 2 "Connectors_Sparkfun:1X06_NO_SILK" H 7150 4150 50  0001 C CNN
F 3 "" H 7150 4150 50  0001 C CNN
	1    7150 4150
	-1   0    0    1   
$EndComp
Text GLabel 7350 4250 2    60   BiDi ~ 0
SCL
Text GLabel 7350 4150 2    60   BiDi ~ 0
SDA
Text GLabel 7350 3750 2    60   Input ~ 0
SW0
Text GLabel 7350 3650 2    60   Input ~ 0
SW1
Text GLabel 3650 3450 0    60   BiDi ~ 0
SDA
Text GLabel 3650 3550 0    60   BiDi ~ 0
SCL
$Comp
L C_Small C2
U 1 1 5A510ABA
P 5350 4250
F 0 "C2" H 5360 4320 50  0000 L CNN
F 1 "0.1µF" H 5360 4170 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5350 4250 50  0001 C CNN
F 3 "" H 5350 4250 50  0001 C CNN
	1    5350 4250
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR01
U 1 1 5A510C1C
P 7500 4450
F 0 "#PWR01" H 7500 4200 50  0001 C CNN
F 1 "GNDD" H 7500 4325 50  0000 C CNN
F 2 "" H 7500 4450 50  0001 C CNN
F 3 "" H 7500 4450 50  0001 C CNN
	1    7500 4450
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR02
U 1 1 5A510CF0
P 5350 3350
F 0 "#PWR02" H 5350 3200 50  0001 C CNN
F 1 "+3V3" H 5350 3490 50  0000 C CNN
F 2 "" H 5350 3350 50  0001 C CNN
F 3 "" H 5350 3350 50  0001 C CNN
	1    5350 3350
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR03
U 1 1 5A510D78
P 5350 4450
F 0 "#PWR03" H 5350 4200 50  0001 C CNN
F 1 "GNDD" H 5350 4325 50  0000 C CNN
F 2 "" H 5350 4450 50  0001 C CNN
F 3 "" H 5350 4450 50  0001 C CNN
	1    5350 4450
	1    0    0    -1  
$EndComp
$Comp
L R_Small R1
U 1 1 5A510E25
P 3250 3650
F 0 "R1" H 3280 3670 50  0000 L CNN
F 1 "10K" H 3280 3610 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 3250 3650 50  0001 C CNN
F 3 "" H 3250 3650 50  0001 C CNN
	1    3250 3650
	0    1    1    0   
$EndComp
$Comp
L R_Small R2
U 1 1 5A510EFB
P 5050 3650
F 0 "R2" H 5080 3670 50  0000 L CNN
F 1 "10K" H 5080 3610 50  0000 L CNN
F 2 "Resistors_SMD:R_0603" H 5050 3650 50  0001 C CNN
F 3 "" H 5050 3650 50  0001 C CNN
	1    5050 3650
	0    1    -1   0   
$EndComp
$Comp
L +3V3 #PWR04
U 1 1 5A51114D
P 3050 3500
F 0 "#PWR04" H 3050 3350 50  0001 C CNN
F 1 "+3V3" H 3050 3640 50  0000 C CNN
F 2 "" H 3050 3500 50  0001 C CNN
F 3 "" H 3050 3500 50  0001 C CNN
	1    3050 3500
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5A5111E7
P 5050 4250
F 0 "C1" H 5060 4320 50  0000 L CNN
F 1 "0.1µF" H 5060 4170 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5050 4250 50  0001 C CNN
F 3 "" H 5050 4250 50  0001 C CNN
	1    5050 4250
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR05
U 1 1 5A51121A
P 5050 4450
F 0 "#PWR05" H 5050 4200 50  0001 C CNN
F 1 "GNDD" H 5050 4325 50  0000 C CNN
F 2 "" H 5050 4450 50  0001 C CNN
F 3 "" H 5050 4450 50  0001 C CNN
	1    5050 4450
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR06
U 1 1 5A51141C
P 3550 4450
F 0 "#PWR06" H 3550 4200 50  0001 C CNN
F 1 "GNDD" H 3550 4325 50  0000 C CNN
F 2 "" H 3550 4450 50  0001 C CNN
F 3 "" H 3550 4450 50  0001 C CNN
	1    3550 4450
	1    0    0    -1  
$EndComp
NoConn ~ 4850 3450
NoConn ~ 4850 3550
NoConn ~ 4850 3850
NoConn ~ 3650 3950
NoConn ~ 3650 3750
$Comp
L GNDD #PWR07
U 1 1 5A51154C
P 3250 4450
F 0 "#PWR07" H 3250 4200 50  0001 C CNN
F 1 "GNDD" H 3250 4325 50  0000 C CNN
F 2 "" H 3250 4450 50  0001 C CNN
F 3 "" H 3250 4450 50  0001 C CNN
	1    3250 4450
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST_SMD S2
U 1 1 5A526E14
P 4450 1800
F 0 "S2" H 4250 1850 45  0000 L BNN
F 1 "B3F" H 4250 1600 45  0000 L BNN
F 2 "air:SW_TH_Tactile_Omron_B3F-40xx" H 4450 2050 20  0001 C CNN
F 3 "" H 4450 1800 50  0001 C CNN
	1    4450 1800
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST_SMD S4
U 1 1 5A526E91
P 3150 1800
F 0 "S4" H 2950 1850 45  0000 L BNN
F 1 "EVQQ2" H 2950 1600 45  0000 L BNN
F 2 "air:SW_SPST_EVQQ2" H 3150 2050 20  0001 C CNN
F 3 "" H 3150 1800 50  0001 C CNN
	1    3150 1800
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST_SMD S3
U 1 1 5A526EC6
P 3150 1300
F 0 "S3" H 2950 1350 45  0000 L BNN
F 1 "EVQQ2" H 2950 1100 45  0000 L BNN
F 2 "air:SW_SPST_EVQQ2" H 3150 1550 20  0001 C CNN
F 3 "" H 3150 1300 50  0001 C CNN
	1    3150 1300
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST_SMD S1
U 1 1 5A526F1A
P 4450 1300
F 0 "S1" H 4250 1350 45  0000 L BNN
F 1 "B3F" H 4250 1100 45  0000 L BNN
F 2 "air:SW_TH_Tactile_Omron_B3F-40xx" H 4450 1550 20  0001 C CNN
F 3 "" H 4450 1300 50  0001 C CNN
	1    4450 1300
	1    0    0    -1  
$EndComp
Text GLabel 4800 1800 2    60   Input ~ 0
SW0
Text GLabel 4800 1300 2    60   Input ~ 0
SW1
$Comp
L +3V3 #PWR08
U 1 1 5A527199
P 4100 1000
F 0 "#PWR08" H 4100 850 50  0001 C CNN
F 1 "+3V3" H 4100 1140 50  0000 C CNN
F 2 "" H 4100 1000 50  0001 C CNN
F 3 "" H 4100 1000 50  0001 C CNN
	1    4100 1000
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR09
U 1 1 5A5281B4
P 5400 1000
F 0 "#PWR09" H 5400 850 50  0001 C CNN
F 1 "+3V3" H 5400 1140 50  0000 C CNN
F 2 "" H 5400 1000 50  0001 C CNN
F 3 "" H 5400 1000 50  0001 C CNN
	1    5400 1000
	1    0    0    -1  
$EndComp
$Comp
L SW S6
U 1 1 5A5281EE
P 5850 1300
F 0 "S6" H 5650 1350 45  0000 L BNN
F 1 "Ultramec" H 5850 1200 45  0000 C BNN
F 2 "air:sw_ultramec_6c" H 5850 1500 20  0001 C CNN
F 3 "" H 5650 1400 50  0001 C CNN
	1    5850 1300
	1    0    0    -1  
$EndComp
$Comp
L SW S7
U 1 1 5A528246
P 5850 1800
F 0 "S7" H 5650 1850 45  0000 L BNN
F 1 "Ultramec" H 5850 1700 45  0000 C BNN
F 2 "air:sw_ultramec_6c" H 5850 2000 20  0001 C CNN
F 3 "" H 5650 1900 50  0001 C CNN
	1    5850 1800
	1    0    0    -1  
$EndComp
Text GLabel 6200 1300 2    60   Input ~ 0
SW1
Text GLabel 6200 1800 2    60   Input ~ 0
SW0
$Comp
L +3V3 #PWR010
U 1 1 5A5658E9
P 2800 1000
F 0 "#PWR010" H 2800 850 50  0001 C CNN
F 1 "+3V3" H 2800 1140 50  0000 C CNN
F 2 "" H 2800 1000 50  0001 C CNN
F 3 "" H 2800 1000 50  0001 C CNN
	1    2800 1000
	1    0    0    -1  
$EndComp
Text GLabel 3500 1800 2    60   Input ~ 0
SW0
Text GLabel 3500 1300 2    60   Input ~ 0
SW1
Text GLabel 7350 3550 2    60   Input ~ 0
INT
Text GLabel 4850 3950 2    60   Input ~ 0
INT
$Comp
L DRV2650L U2
U 1 1 5A567EA1
P 8050 1100
F 0 "U2" H 8050 1275 60  0000 C CNN
F 1 "DRV2650L" H 8050 1175 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-10_3x3mm_Pitch0.5mm" H 8050 1100 60  0001 C CNN
F 3 "" H 8050 1100 60  0001 C CNN
	1    8050 1100
	1    0    0    -1  
$EndComp
Text GLabel 7600 1400 0    60   BiDi ~ 0
SDA
Text GLabel 7600 1300 0    60   BiDi ~ 0
SCL
$Comp
L C_Small C4
U 1 1 5A567FDB
P 9250 1450
F 0 "C4" H 9260 1520 50  0000 L CNN
F 1 "0.1µF" H 9260 1370 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 9250 1450 50  0001 C CNN
F 3 "" H 9250 1450 50  0001 C CNN
	1    9250 1450
	1    0    0    -1  
$EndComp
$Comp
L C_Small C3
U 1 1 5A56806D
P 6950 1450
F 0 "C3" H 6960 1520 50  0000 L CNN
F 1 "1µF" H 6960 1370 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6950 1450 50  0001 C CNN
F 3 "" H 6950 1450 50  0001 C CNN
	1    6950 1450
	-1   0    0    1   
$EndComp
$Comp
L GNDD #PWR011
U 1 1 5A568879
P 9250 1700
F 0 "#PWR011" H 9250 1450 50  0001 C CNN
F 1 "GNDD" H 9250 1575 50  0000 C CNN
F 2 "" H 9250 1700 50  0001 C CNN
F 3 "" H 9250 1700 50  0001 C CNN
	1    9250 1700
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR012
U 1 1 5A5688D4
P 6950 1700
F 0 "#PWR012" H 6950 1450 50  0001 C CNN
F 1 "GNDD" H 6950 1575 50  0000 C CNN
F 2 "" H 6950 1700 50  0001 C CNN
F 3 "" H 6950 1700 50  0001 C CNN
	1    6950 1700
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR013
U 1 1 5A568A21
P 7150 1100
F 0 "#PWR013" H 7150 950 50  0001 C CNN
F 1 "+3V3" H 7150 1240 50  0000 C CNN
F 2 "" H 7150 1100 50  0001 C CNN
F 3 "" H 7150 1100 50  0001 C CNN
	1    7150 1100
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR014
U 1 1 5A568B72
P 9000 1700
F 0 "#PWR014" H 9000 1450 50  0001 C CNN
F 1 "GNDD" H 9000 1575 50  0000 C CNN
F 2 "" H 9000 1700 50  0001 C CNN
F 3 "" H 9000 1700 50  0001 C CNN
	1    9000 1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J2
U 1 1 5A568D09
P 10100 1700
F 0 "J2" H 10100 1800 50  0000 C CNN
F 1 "Conn_01x02" H 10100 1500 50  0000 C CNN
F 2 "Wire_Connections_Bridges:WireConnection_0.80mmDrill" H 10100 1700 50  0001 C CNN
F 3 "" H 10100 1700 50  0001 C CNN
	1    10100 1700
	1    0    0    1   
$EndComp
Text GLabel 8550 1300 2    60   Output ~ 0
OUT-
Text GLabel 8550 1500 2    60   Output ~ 0
OUT+
$Comp
L +3V3 #PWR015
U 1 1 5A569139
P 9250 1050
F 0 "#PWR015" H 9250 900 50  0001 C CNN
F 1 "+3V3" H 9250 1190 50  0000 C CNN
F 2 "" H 9250 1050 50  0001 C CNN
F 3 "" H 9250 1050 50  0001 C CNN
	1    9250 1050
	1    0    0    -1  
$EndComp
Text GLabel 9900 1600 0    60   Input ~ 0
OUT-
Text GLabel 9900 1700 0    60   Input ~ 0
OUT+
$Comp
L +3V3 #PWR016
U 1 1 5A569D74
P 7500 3350
F 0 "#PWR016" H 7500 3200 50  0001 C CNN
F 1 "+3V3" H 7500 3490 50  0000 C CNN
F 2 "" H 7500 3350 50  0001 C CNN
F 3 "" H 7500 3350 50  0001 C CNN
	1    7500 3350
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR017
U 1 1 5A56A748
P 7450 1700
F 0 "#PWR017" H 7450 1450 50  0001 C CNN
F 1 "GNDD" H 7450 1575 50  0000 C CNN
F 2 "" H 7450 1700 50  0001 C CNN
F 3 "" H 7450 1700 50  0001 C CNN
	1    7450 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1500 7600 1500
Wire Wire Line
	7450 1700 7450 1500
Connection ~ 8900 1200
Wire Wire Line
	8900 1600 8900 1200
Wire Wire Line
	8550 1600 8900 1600
Wire Wire Line
	7150 1100 7150 1600
Wire Wire Line
	6950 1200 7600 1200
Wire Wire Line
	6950 1350 6950 1200
Wire Wire Line
	6950 1550 6950 1700
Wire Wire Line
	8550 1200 9250 1200
Wire Wire Line
	9250 1550 9250 1700
Connection ~ 9250 1200
Wire Wire Line
	9250 1050 9250 1350
Wire Wire Line
	9000 1400 9000 1700
Wire Wire Line
	8550 1400 9000 1400
Wire Wire Line
	7150 1600 7600 1600
Wire Wire Line
	7500 4350 7500 4450
Wire Wire Line
	7350 4350 7500 4350
Wire Wire Line
	7500 3450 7500 3350
Wire Wire Line
	7350 3450 7500 3450
Wire Wire Line
	6050 1300 6200 1300
Wire Wire Line
	6050 1800 6200 1800
Connection ~ 5400 1300
Wire Wire Line
	5400 1800 5650 1800
Wire Wire Line
	5400 1300 5650 1300
Wire Wire Line
	5400 1000 5400 1800
Connection ~ 4100 1300
Wire Wire Line
	4100 1300 4250 1300
Connection ~ 2800 1300
Wire Wire Line
	4100 1800 4250 1800
Wire Wire Line
	2800 1300 2950 1300
Wire Wire Line
	2800 1000 2800 1800
Wire Wire Line
	4100 1000 4100 1800
Wire Wire Line
	2800 1800 2950 1800
Wire Wire Line
	3500 1800 3350 1800
Wire Wire Line
	3350 1300 3500 1300
Wire Wire Line
	4800 1800 4650 1800
Wire Wire Line
	4650 1300 4800 1300
Wire Wire Line
	3250 3850 3650 3850
Wire Wire Line
	3250 4450 3250 3850
Wire Wire Line
	3550 4050 3650 4050
Wire Wire Line
	3550 4450 3550 4050
Wire Wire Line
	3050 3650 3050 3500
Wire Wire Line
	3150 3650 3050 3650
Connection ~ 5350 3650
Wire Wire Line
	5150 3650 5350 3650
Wire Wire Line
	5050 4450 5050 4350
Wire Wire Line
	5050 3750 5050 4150
Wire Wire Line
	4850 3750 5050 3750
Wire Wire Line
	3350 3650 3650 3650
Wire Wire Line
	4850 3650 4950 3650
Wire Wire Line
	5350 4450 5350 4350
Connection ~ 5350 4050
Wire Wire Line
	5350 4050 4850 4050
Wire Wire Line
	5350 3350 5350 4150
NoConn ~ 7350 4050
$EndSCHEMATC
