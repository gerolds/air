# Pickup Hardware
KiCad EDA project files (schematics and PCB layouts).

### Contents
- `/proto_*` prototype versions

### Interesting Files
- `/proto_03E/air_03E.sch` schematic of the current *working* prototype.
- `/proto_03E/air_03E.kicad_pcb` PCB layout of the current *working* prototype.

### Build
The PCBs layouts can be fabricated by [https://aisler.net/](AISLER B.V.). Note: these are 4-layer designs with a general 5mil/6mil accuracy requirement.

__Target:__ PCB Fabrication
__Toolchain:__ KiCad