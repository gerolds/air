// Protocol
// accel_left -> outlet 0
// accel_right -> outlet 1

outlets = 2

function anything()
{
	var a = arrayfromargs(messagename, arguments);
	
	if (a[0] == "/accel_left"){
		outlet(0, a);
	}else{
		outlet(1, a);
	}
}