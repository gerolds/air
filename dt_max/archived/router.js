// ROUTER:
// Sends the OSC selected OSC Messages with modified 
// values to specific Outlets and thus to Max Objects

comp = ['freq', 'volume', 'attack', 'decay', 'tone', 'trigger'];

inlets = 1;
outlets = comp.length;

// ====================================
// Configure messages and channels here  
// { message : [ <component>, <add>, <mult>, <elem> ] }
// ====================================
router = {
	'/r/imu/gyro' 	: ['freq', 400, 1, 2],
	'/l/imu/accel' 	: ['attack', 180, 1/36, 2],
	'/r/pressure' 	: ['tone', 0, 10, 1],
	'/post/velocity': ['trigger', 0, 1/300, 0]
}

function anything(){
	// check if OSC message is defined and
	// add to it and multiply it
	try
	{
		target_component = router[messagename][0];
	}
	catch(err)
	{
		if (err.name == 'TypeError'){
			post(messagename + " not defined in routing!");
			return;
		}
		else{
			throw err;
		}
	}
			
	target_outlet = comp.indexOf(target_component);
	
	// stop method if message not defined
	if (target_outlet == -1){ return; }
	
	value = arguments[router[messagename][3]] 
						* router[messagename][2] 
						+ router[messagename][1];
						
	
	
	outlet(target_outlet, value);
}
