{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 0,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 1869.0, 702.0, 117.0, 206.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "preset", "int", "preset", "int" ],
					"patching_rect" : [ 520.0, 318.0, 31.0, 32.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 56.375, 4.900146, 29.0, 29.0 ],
					"preset_data" : [ 						{
							"number" : 1,
							"data" : [ 4, "obj-76", "function", "clear", 8, "obj-76", "function", "add_with_curve", 0.0, 0.0, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.505376, 0.0, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.634409, 0.064209, 2, 0.0, 8, "obj-76", "function", "add_with_curve", 1.0, 0.188423, 0, 0.0, 5, "obj-76", "function", "domain", 1.0, 6, "obj-76", "function", "range", 0.0, 1.0, 5, "obj-76", "function", "mode", 1, 5, "obj-93", "toggle", "int", 1 ]
						}
, 						{
							"number" : 2,
							"data" : [ 4, "obj-76", "function", "clear", 8, "obj-76", "function", "add_with_curve", 0.0, 0.209475, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.350806, 0.062107, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.505376, 0.0, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.634409, 0.064209, 2, 0.0, 8, "obj-76", "function", "add_with_curve", 1.0, 0.272633, 0, 0.0, 5, "obj-76", "function", "domain", 1.0, 6, "obj-76", "function", "range", 0.0, 1.0, 5, "obj-76", "function", "mode", 1, 5, "obj-93", "toggle", "int", 1 ]
						}
, 						{
							"number" : 3,
							"data" : [ 4, "obj-76", "function", "clear", 8, "obj-76", "function", "add_with_curve", 0.0, 0.714738, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.458333, 0.998949, 2, 0.0, 8, "obj-76", "function", "add_with_curve", 1.0, 1.0, 0, 0.0, 5, "obj-76", "function", "domain", 1.0, 6, "obj-76", "function", "range", 0.0, 1.0, 5, "obj-76", "function", "mode", 1, 5, "obj-93", "toggle", "int", 1 ]
						}
, 						{
							"number" : 4,
							"data" : [ 4, "obj-76", "function", "clear", 8, "obj-76", "function", "add_with_curve", 0.0, 0.56737, 0, 0.0, 8, "obj-76", "function", "add_with_curve", 0.598118, 1.0, 2, 0.0, 8, "obj-76", "function", "add_with_curve", 1.0, 1.0, 0, 0.0, 5, "obj-76", "function", "domain", 1.0, 6, "obj-76", "function", "range", 0.0, 1.0, 5, "obj-76", "function", "mode", 1, 5, "obj-93", "toggle", "int", 1 ]
						}
 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 202.0, 214.149902, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 202.0, 181.149902, 60.0, 22.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 328.0, 280.5, 119.0, 22.0 ],
					"style" : "",
					"text" : "scale -1. 1. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 273.5, 197.349854, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.375, 9.900146, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 289.0, 236.0, 39.0, 32.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"format" : 6,
					"id" : "obj-100",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : -1.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 309.0, 181.149902, 64.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-101",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 289.0, 323.800049, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.248788, 0.249934, 0.242723, 1.0 ],
					"contdata" : 1,
					"id" : "obj-88",
					"ignoreclick" : 1,
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 571.0, 594.599854, 14.75, 30.75 ],
					"presentation" : 1,
					"presentation_rect" : [ 89.375, 178.900146, 21.0, 22.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"slidercolor" : [ 0.262392, 0.958854, 0.88746, 1.0 ],
					"spacing" : 1,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.248788, 0.249934, 0.242723, 1.0 ],
					"contdata" : 1,
					"drawpeaks" : 1,
					"id" : "obj-87",
					"ignoreclick" : 1,
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 571.0, 633.349854, 91.0, 30.75 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.375, 178.900146, 84.0, 22.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 5,
					"slidercolor" : [ 0.262392, 0.958854, 0.88746, 1.0 ],
					"spacing" : 2,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-86",
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 442.875, 631.349854, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 442.875, 350.900146, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 31.375, 9.900146, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 442.875, 380.5, 41.0, 22.0 ],
					"style" : "",
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"addpoints_with_curve" : [ 0.0, 0.0, 0, 0.0, 0.505376, 0.0, 0, 0.0, 0.634409, 0.064209, 2, 0.0, 1.0, 0.188423, 0, 0.0 ],
					"autosustain" : 1,
					"bgcolor" : [ 0.248788, 0.249934, 0.242723, 1.0 ],
					"domain" : 1.0,
					"grid" : 3,
					"gridcolor" : [ 0.520598, 0.52724, 0.527104, 1.0 ],
					"gridstep_x" : 0.5,
					"gridstep_y" : 0.5,
					"id" : "obj-76",
					"linecolor" : [ 0.262392, 0.958854, 0.88746, 1.0 ],
					"linethickness" : 3.0,
					"maxclass" : "function",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 442.875, 432.599854, 105.0, 120.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.375, 58.900146, 105.0, 120.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-62",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 309.0, 95.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.248788, 0.249934, 0.242723, 1.0 ],
					"contdata" : 1,
					"id" : "obj-34",
					"ignoreclick" : 1,
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 157.0, 390.300049, 14.375, 23.899902 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.375, 35.900146, 21.0, 23.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"slidercolor" : [ 0.262392, 0.958854, 0.88746, 1.0 ],
					"spacing" : 1,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Fira Code",
					"format" : 6,
					"id" : "obj-7",
					"ignoreclick" : 1,
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 571.0, 678.349854, 63.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.248788, 0.249934, 0.242723, 1.0 ],
					"contdata" : 1,
					"drawpeaks" : 1,
					"id" : "obj-39",
					"ignoreclick" : 1,
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 157.0, 424.599854, 91.0, 23.899902 ],
					"presentation" : 1,
					"presentation_rect" : [ 26.375, 35.900146, 84.0, 23.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"slidercolor" : [ 0.262392, 0.958854, 0.88746, 1.0 ],
					"spacing" : 2,
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"angle" : 249.939428,
					"grad1" : [ 0.447059, 0.823529, 0.772549, 1.0 ],
					"grad2" : [ 0.447059, 0.823529, 0.772549, 1.0 ],
					"id" : "obj-105",
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 714.0, 285.5, 113.75, 182.849854 ],
					"presentation" : 1,
					"presentation_rect" : [ -9.625, -23.099854, 135.0, 241.0 ],
					"proportion" : 0.39,
					"pt1" : [ 0.5, 0.05 ],
					"pt2" : [ 0.143564, 1.026087 ],
					"style" : ""
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-97", 1 ]
				}

			}
 ]
	}

}
