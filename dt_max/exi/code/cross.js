inlets = 2;
outlets = 1;

var a = new Array(3);
var b = new Array(3);
var r = new Array(3);

function list(r) {
	if (inlet == 0) {
 		a = arguments;
	}
	if (inlet == 1) {
		b = arguments;
	}
	
	//outlet(0, q[0], q[1], q[2]);
	bang();
}

// rotates vector v (input 1) by quaternion q (input 0)
function bang() {
	r[0] = a[1] * b[2] - a[2] * b[1];
	r[1] = a[2] * b[0] - a[0] * b[2];
	r[2] = a[0] * b[1] - a[1] * b[0];

	outlet(0, r[0], r[1], r[2]);
}