inlets = 2;
outlets = 1;

var q = new Array(4);
var v = new Array(3);
var rv = new Array(3);

function list(r) {
	if (inlet == 0) {
 		q = arguments;
	}
	if (inlet == 1) {
		v = arguments;
	}
	
	//outlet(0, q[0], q[1], q[2]);
	bang();
}

// rotates vector v (input 1) by quaternion q (input 0)
function bang() {
	m11 = 1 - 2 * q[2] * q[2] - 2 * q[3] * q[3];
	m12 = 2 * (q[1] * q[2] + q[0] * q[3]);
	m13 = 2 * (q[1] * q[3] - q[0] * q[2]);
	m21 = 2 * (q[1] * q[2] - q[0] * q[3]);
	m22 = 1 - 2 * q[1] * q[1] - 2 * q[3] * q[3];
	m23 = 2 * (q[2] * q[3] + q[0] * q[1]);
	m31 = 2 * (q[1] * q[3] + q[0] * q[2]);
	m32 = 2 * (q[2] * q[3] - q[0] * q[1]);
	m33 = 1 - 2 * q[1] * q[1] - 2 * q[2] * q[2];
	rv[0] = v[0] * m11 + v[1] * m12 + v[2] * m13;
	rv[1] = v[0] * m21 + v[1] * m22 + v[2] * m23;
	rv[2] = v[0] * m31 + v[1] * m32 + v[2] * m33;
	outlet(0, rv[0], rv[1], rv[2]);
}