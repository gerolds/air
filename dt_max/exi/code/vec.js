inlets = 2;
outlets = 1;

var quat = [0,0,0,0];
var vec = [0,0,0];
function bang() {
    num = quat[0] * 2.0;
    num2 = quat[1] * 2.0;
    num3 = quat[2] * 2.0;
    num4 = quat[0] * num;
    num5 = quat[1] * num2;
    num6 = quat[2] * num3;
    num7 = quat[0] * num2;
    num8 = quat[0] * num3;
    num9 = quat[1] * num3;
    num10 = quat[3] * num;
    num11 = quat[3] * num2;
    num12 = quat[3] * num3;
    result = [0,0,0];
    result[0] = (1.0 - (num5 + num6)) * vec[0] + (num7 - num12) * vec[1] + (num8 + num11) * vec[2];
    result[1] = (num7 + num12) * vec[0] + (1.0 - (num4 + num6)) * vec[1] + (num9 - num10) * vec[2];
    result[2] = (num8 - num11) * vec[0] + (num9 + num10) * vec[2] + (1.0 - (num4 + num5)) * vec[2];
//	post(quat, ", ", vec);
	outlet(0, result);
}

function list(a, b, c, d) {
	if (inlet==0) quat = [a,b,c,d];
	if (inlet==1) vec = [a,b,c];
	bang();
}