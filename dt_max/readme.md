# max/MSP Prototypes and Mockups.

### Contents
- `exi` - EXI Exploration Interface. Active development. Developed in Max 7 on macOS 10.13. May require proprietary Fonts and VST plug-ins (not included).
- `archive` - **DEPRECATED** - Old patches developed over the course of the project. Undocumented.

### Interesting files
- `exi/exi/patchers/exi.maxpat` -  The core of the project. Includes a router with many channels, whose job is to modify incoming OSC Messages and map them to synthesizer components.
- `archive/air_additive.maxpat` - A sketch of an additive synthesizer.

__Target:__ Desktop/Windows/macOS  
__Toolchain:__ Max
