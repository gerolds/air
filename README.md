# Air — A musical instrument (almost) free of physical constraints

*How can we create a musical instruments that is free of the constraints a real instrument's physicality imposes? How can we turn any thing into an instrument, or even just air? How can intuitive motion be translated into music?*

[Concept video](https://vimeo.com/254121214) (Vimeo)

Over the course of three months during the autumn/winter 2017/18 we developed an integrated hard- and software system that allows the user to explore sound and music creation via dynamic hand and arm motion. The system consists of three parts, two wireless motion pickup devices, one for each hand, and a GUI application, called EXI, for filtering, processing, shaping the raw sensor data from the pickups and connecting the results to the control channels of a wavetable synthesizer. EXI is built using Cycling74's Max while the Pickups are a custom hardware solution that was developed as part of the project with firmware built on the Teensy \& Arduino frameworks. The current state of the system prototype is promising. It seems possible to develop the project into a marketable product within a reasonable time frame. No technological, economic or design obstacles have emerged so far.

This project took place as part of Coding IXD 2017/18 at Freie Universität Berlin in collaboration with Weißensee Kunsthochschule Berlin on the topic of Mixed Reality. Coding IxD  challenged the participating teams to pose a concrete design question  inside broader topic of Mixed Reality and develop a prototype exploring said question.

### Team
* Gerold Schneider
* Ylenia Gortana
* Emil Milanov

### Conventions in this repo ###

Top-level folder names follow the pattern:

```./[target]_[toolchain]```

where

* ```[target]``` aka. target platform (microcontroller or OS). Might be, for example: ```rpi``` (Raspbian), ```uno``` (Arduino UNO), ```teensy``` (Teensy), ```macos``` (macOS), ```ix``` (generic Unix/Linux), ```dt``` (Desktop/Notebook running Win/Linux/macOS) ...
* ```[toolchain]``` the compiler/ide/toolchain/environment in which the project is intended to be built, debugged, deployed or executed. Might be, for example: ```py``` (Python), ```jvm``` (Java Virtual Machine), ```max``` (Max/MSP), ```c``` (Nativ C code), ```processing``` (Processing IDE), ```arduino``` (Arduino IDE) ...